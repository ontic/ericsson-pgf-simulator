This is the source code to creat the docker image for the **PGF Mitigation Plan
Simulator**.

To build the image:

```bash
	docker build -t pgf-simulator .
```

To run the image:

```bash
	docker run -p 8008:8008 -d simulator
```

You can now interact with the simulator at localhost:8008
