////////////////////////////////////////////////////////////////////////////////
////                                                                        //// 
//// COPYRIGHT (c) Ericsson AB 2015                                         //// 
////                                                                        //// 
//// The copyright to the computer Program(s) herein is the                 //// 
//// property of Ericsson AB, Sweden. The program(s) may be                 //// 
//// used and or copied only with the written permission of                 //// 
//// Ericsson AB, or in accordance with the terms and conditions            //// 
//// stipulated in the agreement contract under which the                   //// 
//// program(s) have been supplied.                                         //// 
////                                                                        //// 
////////////////////////////////////////////////////////////////////////////////

//// Version
var version = '2015-11-17 11:21:45.33'

//// Chart drawing
var data_input = {};       // Main container for simulation results: it is an object with the KPI name as key and 
                           //an array as value
                           // There are as many arrays as customer groups in the simulation results
                           // Each array is, in itself, an array of arrays. The firs element of these arrays is the 
                           //customer segment name. The remaining ones are the KPI gain values for each of the plans
var data_input_av = {};    // Following the same structure as data_input, it stores the average KPI gain
var data_input_wav = {};   // Following the same structure as data_input, it stores the weighted average KPI gain

var head_row = [];         // Variables containing the heading row for data_input and the like (it contains the customer
                           //segment names)
head_row.push('Customer segment');
    
// mappings to translate parameters received from the JSON file
var kpi_map =       {
                        vfr: "Video Freeze Rate", 
                        va: "Video Accessibility"
                    };
var group_name_map ={
                        gold: "Gold", 
                        silver_domestic: "Silver Domestic", 
                        bronze_domestic: "Bronze Domestic", 
                        silver_corporate: "Silver Corporate", 
                        bronze_corporate: "Bronze Corporate", 
                        bronze_young: "Bronze Young"
                    };

// miscellaneous mappings
var kpi_names = [];
var group_shares = {};
var plan_number = 0;

// this function is called just once and creates data structures from the information passed on by the JSON file. Said
//structures will be used later on to create a chart
function capture_info (JSON_chart_object) {
    // First, we extract the available subscriber groups (just in case any of the plans have not a result for a given group)
    
    // iteration through each plan container within the results array (results comprises as many plan objects as simulated plans)
    // in order to extract plan and KPI names
    plan_number = JSON_chart_object.results.length;
    var group_shares = {};
    $.each(JSON_chart_object.results, function( plan_index, plan_content ) {
        var plan_name = plan_content.plan
        var plan_name_string = plan_name;

        head_row.push(plan_name_string);        // header creation with plan names
        //console.log ("Plan with name " + plan_content.plan);

        // iteration through each kpi container within a plan container (kpis comprises as many kpi objects as simulated KPI)
        $.each(plan_content.kpis, function ( kpi_index, kpi_content ) {
            // KPI name array population
            if (kpi_names.indexOf(kpi_content.kpi_name) === -1) {
                kpi_names.push(kpi_content.kpi_name);
                //console.log ('KPI with name ' + kpi_content.kpi_name)
            }
            // iteration through each group container within kpi container
        });
        $.each(plan_content.groups, function ( group_index, group_content ) {
            //console.log ('Group name: ' + group_content.group + ' in plan ' + plan_content.plan + ' (share: '+group_content.share+')');
            if (group_shares.hasOwnProperty (group_content.group)) {
                //console.log ('key update ('+ group_content.group +')')
                group_shares[group_content.group] = group_shares[group_content.group] + group_content.share;
            } else {
                //console.log ('key initialization ('+ group_content.group +')')
                group_shares [group_content.group] = group_content.share;
            }
        });
        
    });

    // initialization of data structures used as input to visualization (matrices containing KPI gain)
    $.each(kpi_names, function (kpi_index, kpi_short_name) {
        var kpi_name = kpi_map[kpi_short_name]; // long KPI name needed
        data_input[kpi_name] = [];              // an array for each KPI within data_input
        data_input_av[kpi_name] = [];
        data_input_wav[kpi_name] = [];
        //console.log (kpi_name);
        // array values are set to zero
        var i = 0;
        $.each(group_shares, function (group_name, group_share) {
            data_input[kpi_name][i] = [];
            data_input[kpi_name][i][0] = group_name_map[group_name];
            data_input_av[kpi_name][i] = [];
            data_input_av[kpi_name][i][0] = group_name_map[group_name];
            data_input_wav[kpi_name][i] = [];
            data_input_wav[kpi_name][i][0] = group_name_map[group_name];

            for (j=0; j<plan_number; j++) {
                data_input[kpi_name][i][j+1] = 0;

                data_input_av[kpi_name][i][j+1] = 0;
                data_input_wav[kpi_name][i][j+1] = 0;
            }
            i++;
        });
    });

    // fill of data structures used as input to visualization (matrices containing KPI gain)
    $.each(JSON_chart_object.results, function (plan_index, plan_contents) {
        $.each(plan_contents.kpis, function (kpi_index, kpi_contents) {
            var overall_delta_value = 0;
            var overall_weighted_value = 0;
            
            if (kpi_contents.value_type === "per_cent") {
                overall_delta_value = kpi_contents.delta_value;
                overall_weighted_value = kpi_contents.weighted_delta_value;
            } else if (kpi_contents.value_type === "per_one") {
                overall_delta_value = kpi_contents.delta_value * 100;
                overall_weighted_value = kpi_contents.weighted_delta_value * 100;
            }
            
            // here I've got the plan name (plan_contents.plan), the number of plans (plan_number) the kpi name (kpi_contents.kpi_name)
            // so that I should be able to fill all the values related to data_input_av[kpi_name] from i=1 to i=plan_number
            var kpi_name = kpi_map[kpi_contents.kpi_name];
            for (j=0; j<Object.keys(group_shares).length; j++) {
                data_input_av[kpi_name][j][plan_index+1] = Math.round(overall_delta_value * 100) / 100;
                data_input_wav[kpi_name][j][plan_index+1] = Math.round(overall_weighted_value * 100) / 100;
            }
            // algorithm
            // - iterate over all the kpi_contents.kpi_groups array -> done
            // - if a policy value set to 'No' is found, look for other item with same group_name in the array -> done
            // - consolidate both items in a single item (taking the share from other location)
            // - replace the first item with the consolidated item
            // - remove the additional item

            // iterate over all the kpi_contents.kpi_groups array
            $.each(kpi_contents.kpi_groups, function (kpi_group_index, kpi_group_contents) {
                // look for an item with the policy value set to 'No' (once found, exit the loop)
                if (kpi_group_contents.policy === 'No') {
                    var group_name = kpi_group_contents.name;
                    // iterate over all the kpi_contents.kpi_groups array
                    $.each(kpi_contents.kpi_groups, function (kpi_group_index2, kpi_group_contents2) {
                        // look for the other item with same group_name in the array 
                        if ((kpi_group_contents2.name === group_name) && (kpi_group_contents2.policy !== 'No')) {
                            var share_yes = 0.0;
                            var share_no = 0.0;
                            // extract shares for both groups
                            $.each(plan_contents.groups, function (group_index, group_content) {
                                if (group_content.group === kpi_group_contents.name) {
                                    if (group_content.policy !== 'no_wifi') {
                                        share_yes = group_content.share;
                                        //console.log ('Share+: ' + group_content.share);
                                    } else {
                                        share_no = group_content.share;
                                        //console.log ('Share: ' + group_content.share);
                                    }
                                }
                            });
                            // consolidate both items in a single item and replace the first one (removing the policy property)
                            delete kpi_group_contents.policy;
                            kpi_group_contents.kpi_gain = ((kpi_group_contents.kpi_gain*share_no) + (kpi_group_contents2.kpi_gain*share_yes)) / (share_yes + share_no);
                            // delete remaining item
                            kpi_contents.kpi_groups.splice(kpi_group_index2, 1);
                            return false;
                        }
                    });
                    return false;
                    //console.log (kpi_group_contents.name);
                }
            });
            
            // iterate over kpi_contents.kpi_groups to fill data_input
            $.each(kpi_contents.kpi_groups, function (kpi_group_index, kpi_group_contents) {
                // here I've got the plan name (plan_contents.plan), the kpi name (kpi_contents.kpi_name), 
                // the group name (kpi_group_contents.name), and the KPI gain value (kpi_group_contents.kpi_gain)
                // so that I should be able to fill all the values related to data_input[kpi_name] from i=1 to i=plan_number
                
                var group_index = get_group_index (kpi_group_contents.name, data_input[kpi_name]);
                
                if (kpi_group_contents.value_type === "per_one") {
                    data_input[kpi_name][group_index][plan_index+1] = kpi_group_contents.kpi_gain * 100;
                } else if (kpi_group_contents.value_type === "per_cent") {
                    data_input[kpi_name][group_index][plan_index+1] = kpi_group_contents.kpi_gain;
                }
                data_input[kpi_name][group_index][plan_index+1] = Math.round(data_input[kpi_name][group_index][plan_index+1] * 100) / 100
            });
        });
    });
}

function draw_tables(chart_type, pane) {
    //console.log('Selected pane is ' + pane);
    // options for each type of chart
    if (DEBUG === true) {    
        $('#JSON_output').html(JSON.stringify(simulation_chart_object));
    }
    var colors = ['#f1595f', '#79c36a', '#599ad3', '#f9a65a', '#9e66ab', '#cd7058', '#d77fb3', '#727272']
    var options = {
        title : '',
        vAxis: {title:"KPI gain (%)", minValue: -100, maxValue: 100},
        hAxis: {title:"Customer segments"},
        height: 400, 
        seriesType: 'bars',
        focusTarget: 'category',
        tooltip: {isHtml: true, trigger: 'selection', ignoreBounds: true},
        };
    var options_av = {
        title : '',
        vAxis: {title:"KPI gain (%)", minValue: -100, maxValue: 100},
        hAxis: {title:"Customer segments"},
        height: 400, 
        seriesType: 'line',
        tooltip: {isHtml: true, trigger: 'selection', ignoreBounds: true},
    };
    var options_wav = $.extend(true, {}, options_av);

    // initial drawing    
    if (pane === undefined) {
        $('#chart_title').html ("<h2>Simulation with ID: " + simulation_chart_object.id + '</h2><p>Date: ' + simulation_chart_object.date +'</p>');

        $.each(kpi_names, function (index, value) {
            var kpi_name = kpi_map[value];
            //console.log (kpi_map[value]);
            var data_reg = new google.visualization.DataTable();
            
            // Definition of column type            
            $.each (head_row, function (i, val) {
                if (i === 0) {
                    data_reg.addColumn('string', val);
                    data_reg.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
                } else {
                    data_reg.addColumn('number', val);
                }
            });

            options.title = kpi_name + ' simulation results';
            options.colors = colors.slice(0, plan_number);

            var div_id = 'JSON_output_' + (index+1);
            var chart = new google.visualization.ComboChart(document.getElementById(div_id));
            
            var data_table = [];
            $.each (data_input[kpi_name], function (i, row) {
                var subs_group_text = '<div style="padding:5px 5px 5px 5px;">\n' +
                                        '<b>' + row [0] + '</b><br/>' + '<table>'
                $.each (row, function (j, item) {
                    if (j !== 0) {
                        var plan_description = get_plan_text(head_row[j]);
                        subs_group_text = subs_group_text + 
                                '<td style="border:0;margin:0;padding:0;white-space: nowrap;">' +
                                '<div class="color-box" style="background-color: '+ colors[j-1] +';"></div>' +
                                '<b>' + head_row[j].replace(' ', '&nbsp;') + '</b>:&nbsp;</td>' + 
                                '<td style="border:0;margin:0;padding:0;;white-space: nowrap;">' + item + '%</td></tr>'  + 
                                '<tr><td colspan="2"><a class="tip">See plan features<span><img class="callout" src="' + image_path + '" />' + plan_description + '</span></a></td></tr>' +
                                '<tr><td colspan="2"><a href="#">Save plan for further use</a></td></tr>'
                    }
                });
                subs_group_text = subs_group_text + '</table>' + '</div>'
                var row_to_copy = $.extend(true, [], row);
                row_to_copy.splice(1, 0, subs_group_text);
                data_table.push(row_to_copy);
            });

            data_reg.addRows(data_table);
            chart.draw(data_reg, options);
            $("#chart_"+(index+1)+"_options").css ('display', 'block');
        });
    } else {
        // subsequent drawings
        var kpi_name = kpi_map[kpi_names[parseInt(pane)-1]];

        var data_reg = new google.visualization.DataTable();
        var data_av  = new google.visualization.DataTable();
        var data_wav = new google.visualization.DataTable();

        // Definition of column type            
        $.each (head_row, function (i, val) {
            if (i === 0) {
                data_reg.addColumn('string', val);
                data_reg.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
                data_av.addColumn('string', val);
                data_wav.addColumn('string', val);
            } else {
                data_reg.addColumn('number', val);
                data_av.addColumn('number', val);
                data_av.addColumn({type: 'string', role: 'annotation'});
                data_av.addColumn({type: 'string', role: 'annotationText', p: {'html':true}});
                data_wav.addColumn('number', val);
                data_wav.addColumn({type: 'string', role: 'annotation'});
                data_wav.addColumn({type: 'string', role: 'annotationText', p: {'html':true}});
            }
        });

        options.title = options_av.title = options_wav.title = kpi_name + ' simulation results';
        options.colors = options_av.colors = options_wav.colors = colors.slice(0, plan_number);
        
        var div_id = 'JSON_output_' + pane;
        var chart = new google.visualization.ComboChart(document.getElementById(div_id));
        var chart_av = new google.visualization.ComboChart(document.getElementById(div_id));
        var chart_wav = new google.visualization.ComboChart(document.getElementById(div_id));

        if (chart_type == 'main') {
            var data_table = [];
            $.each (data_input[kpi_name], function (i, row) {
                var subs_group_text = '<div style="padding:5px 5px 5px 5px;">\n' +
                                        '<b>' + row [0] + '</b><br/>' + '<table>'
                $.each (row, function (j, item) {
                    if (j !== 0) {
                        var plan_description = get_plan_text(head_row[j]);
                        subs_group_text = subs_group_text + 
                                '<td style="border:0;margin:0;padding:0;white-space: nowrap;">' +
                                '<div class="color-box" style="background-color: '+ colors[j-1] +';"></div>' +
                                '<b>' + head_row[j].replace(' ', '&nbsp;') + '</b>:&nbsp;</td>' + 
                                '<td style="border:0;margin:0;padding:0;;white-space: nowrap;">' + item + '%</td></tr>'  + 
                                '<tr><td colspan="2"><a class="tip">See plan features<span><img class="callout" src="' + image_path + '" />' + plan_description + '</span></a></td></tr>' +
                                '<tr><td colspan="2"><a href="#">Save plan for further use</a></td></tr>'
                    }
                });
                subs_group_text = subs_group_text + '</table>' + '</div>'
                var row_to_copy = $.extend(true, [], row);
                row_to_copy.splice(1, 0, subs_group_text);
                data_table.push(row_to_copy);
            });

            data_reg.addRows(data_table);
            chart.draw(data_reg, options);
        } else if (chart_type == 'average') {
            var data_table_av = [];
            $.each (data_input_av[kpi_name], function (i, row) {
                var row_to_copy = $.extend(true, [], row);
                for (j=0; j<(row.length-1); j++) {
                    row_to_copy.splice((j+2*(j+1)), 0, head_row[j+1], head_row[j+1] + ': ' + row_to_copy[(3*j)+1] + '%');
                }
                data_table_av.push(row_to_copy);
            });
            data_av.addRows (data_table_av);
            chart_av.draw(data_av, options_av);

        } else if (chart_type == 'weighted') {
            var data_table_wav = [];
            $.each (data_input_wav[kpi_name], function (i, row) {
                var row_to_copy = $.extend(true, [], row);
                for (j=0; j<(row.length-1); j++) {
                    row_to_copy.splice((j+2*(j+1)), 0, head_row[j+1], head_row[j+1] + ': ' + row_to_copy[(3*j)+1] + '%');
                }
                data_table_wav.push(row_to_copy);
            });
            data_wav.addRows (data_table_wav);
            chart_wav.draw(data_wav, options_wav);
        }

        $("#chart_"+(parseInt(pane))+"_options").css ('display', 'block');

    }
}

//// Event handlers

// Function to choose chart type. It handles the change event associated to ':radio' selector
var choose_chart = function () {
    var pattern = /chart_(.)_.+/;
    pane_id = $(this).attr('id').match(pattern);
    switch ($(this).val()) {
        case "one" :
            draw_tables('main', pane_id[1]);
            break;
        case "two" :
            draw_tables('average', pane_id[1]);
            break;
        case "three" :
            draw_tables('weighted', pane_id[1]);
            break;
        default :
    }
}

//// Auxiliary functions

function get_group_index (group_name1, group_array) {
    var ret_index = NaN;
    var group_code = group_name_map[group_name1];
    $.each(group_array, function (array_index, array_value) {
        var name = array_value[0];
        if (name === group_code) {
            ret_index = array_index;
        }
    });
    return ret_index;
}

function get_plan_text (plan_name) {
    var plan_features = $.grep(simulation_chart_object["results"], function(e){ return e["plan"] == plan_name; });
    var plan_text = '<strong>' + plan_name + '</strong>\n'
    plan_text = plan_text + '<ul>\n'    

    $.each (plan_features[0]["groups"], function (index, value) {
        switch(value["policy"]) {
            case "wifi":
                plan_text = plan_text + '<li><strong>' + value["group"] + '</strong>: Offload to Wifi.\n'
                break;
            case "to3g":
                plan_text = plan_text + '<li><strong>' + value["group"] + '</strong>: Switch to 3G.\n'
                break;
            case "video_acceleration":
                plan_text = plan_text + '<li><strong>' + value["group"] + '</strong>: Video acceleration.\n'
                break;
            case "bandwidth_throttling":
                var bandwidth_text = '';
                switch (value["policy_parameters"]["limit"]) {
                    case 64 :
                        bandwidth_text = '64 kbps';
                        break;
                    case 1000 :
                        bandwidth_text = '1 Mbps';
                        break;
                    case 3000 :
                        bandwidth_text = '3 Mbps';
                        break;
                }
                plan_text = plan_text + '<li><strong>' + value["group"] + '</strong>: Bandwidth throttled to ' + bandwidth_text + '.\n'
                break;
            case "no_traffic_gating":
            case "traffic_gating":
                var gating_text = [value["policy_parameters"]["services"].slice(0, -1).join(', '), value["policy_parameters"]["services"].slice(-1)[0]].join(value["policy_parameters"]["services"].length < 2 ? '' : ' and ');
                gating_text = gating_text.replace('video', 'Video').replace('web_browsing', 'Web Browsing').replace('file_transfer', 'File Transfer');
                plan_text = plan_text + '<li><strong>' + value["group"] + '</strong>: Service banning (' + gating_text + ').\n'
                break;
        }
    });
    
    plan_text = plan_text + '</ul>'
    return plan_text;
}
/*
var toType = function(obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}
*/