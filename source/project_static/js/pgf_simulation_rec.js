////////////////////////////////////////////////////////////////////////////////
////                                                                        //// 
//// COPYRIGHT (c) Ericsson AB 2015                                         //// 
////                                                                        //// 
//// The copyright to the computer Program(s) herein is the                 //// 
//// property of Ericsson AB, Sweden. The program(s) may be                 //// 
//// used and or copied only with the written permission of                 //// 
//// Ericsson AB, or in accordance with the terms and conditions            //// 
//// stipulated in the agreement contract under which the                   //// 
//// program(s) have been supplied.                                         //// 
////                                                                        //// 
////////////////////////////////////////////////////////////////////////////////

//// Auxiliary variables
var progress_bar;
var progress_bar_name = 'progress_bar_rec'
var progress_bar_width = 10;

//// Auxiliary functions

var combine = function(a, min) {
    var fn = function(n, src, got, all) {
        if (n == 0) {
            if (got.length > 0) {
                all[all.length] = got;
            }
            return;
        }
        for (var j = 0; j < src.length; j++) {
            fn(n - 1, src.slice(j + 1), got.concat([src[j]]), all);
        }
        return;
    }
    var all = [];
    for (var i = min; i < a.length; i++) {
        fn(i, a, [], all);
    }
    all.push(a);
    return all;
}

//// Progress bar functions

function show_progress () {
    var div_id_text = '#' + progress_bar_name;
    if (progress_bar_width < 90) {
        progress_bar_width++; 
        $(div_id_text).css("width", progress_bar_width.toString() + '%');
        $(div_id_text).text(progress_bar_width.toString() + '%');
    }
}

function terminate_progress () {
    var div_id_text = '#' + progress_bar_name;
    var dots = [95, 99, 100];
    for (i = 0; i < dots.length; i++) { 
        $(div_id_text).css("width", dots[i].toString() + '%');
        if (dots[i] === 100) {
            $(div_id_text).text('100% complete');
        } else {
            $(div_id_text).text(dots[i].toString() + '%');
        }
    }
    clearInterval(progress_bar);
}

//// Drawing functions

function get_plan_html (plan_object) {
    var KPI_map = {'va': "Video Accessibility", 'vfr': "Video Freeze Rate"};
    var text = '<ul>\n'
    var table = '<table class="table">\n'
    table = table + '<thead>\n<tr><th class="plan_cell">Subscriber Group</th><th class="plan_cell_centered">Suggested policy</th></tr></thead>\n<tbody>'
    var row_counter = 0;
    $.each(plan_object["plan"], function (index, value) {
        row_counter++;
        group_name_raw = value["group"].replace('_', ' ');
        group_name = group_name_raw.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });
        policy_text = '';
        switch (value["policy"]) {
            case 'bandwidth_throttling' :
                switch (value["parameters"]["limit"]) {
                case 3000 :
                    policy_text = policy_text + '<img src="' + IMAGE_DIR + POLICY_IMAGES["bandwidth_throttling_3000"] + '" style="height:50px; margin-left:0;" /><p class="policy_title">Bandwidth throttling<br/>(3 Mbps)</p>'
                    break;
                case 1000 :
                    policy_text = policy_text + '<img src="' + IMAGE_DIR + POLICY_IMAGES["bandwidth_throttling_1000"] + '" style="height:50px; margin-left:0;" /><p class="policy_title">Bandwidth throttling<br/>(1 Mbps)</p>'
                    break;
                case 64 :
                    policy_text = policy_text + '<img src="' + IMAGE_DIR + POLICY_IMAGES["bandwidth_throttling_64"] + '" style="height:50px; margin-left:0;" /><p class="policy_title">Bandwidth throttling<br/>(64 kbps)</p>'
                    break;
                }
                break;
            case 'traffic_gating' :
                var services = [];
                $.each (value["parameters"]["services"], function (i, param) {
                    service_name_raw = param.replace('_', ' ');
                    service_name = service_name_raw.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                        return letter.toUpperCase();
                    });
                    services.push(service_name)
                });
                services_text = services.join(', ')
                if (services.indexOf ("Video") !== -1) {
                    policy_text = policy_text + '<img src="' + IMAGE_DIR + POLICY_IMAGES["video"] + '" style="height:50px; margin-left:0;" />'
                }
                else if (services.indexOf ("File Transfer") !== -1) {
                    policy_text = policy_text + '<img src="' + IMAGE_DIR + POLICY_IMAGES["file_transfer"] + '" style="height:50px; margin-left:0;" />'
                }
                else if (services.indexOf ("Web Browsing") !== -1) {
                    policy_text = policy_text + '<img src="' + IMAGE_DIR + POLICY_IMAGES["web_browsing"] + '" style="height:50px; margin-left:0;" />'
                }
                policy_text = policy_text + '<p class="policy_title">Traffic gating<br/>';
                policy_text = policy_text + ' (' + services_text + ')</p>'
                break;
            case 'to3g' :
                policy_text = '<img src="' + IMAGE_DIR + POLICY_IMAGES["to3G"] + '" style="height:50px; margin-left:0;" /><p class="policy_title">Switch to 3G</p>';
                break;
            case 'wifi' :
                policy_text = '<img src="' + IMAGE_DIR + POLICY_IMAGES["wifi"] + '" style="height:50px; margin-left:0;" /><p class="policy_title">Offload to Wifi</p>';
                break;
            case 'video_acceleration' :
                policy_text = '<img src="' + IMAGE_DIR + POLICY_IMAGES["video_acceleration"] + '" style="height:50px; margin-left:0;" /><p class="policy_title">Video<br/>acceleration</p>';
                break;
            case 'no_policy' :
                policy_text = '<p class="policy_title">No policy</p>';
                break;
            }
        text = text + '<li><strong>' + group_name + '</strong>: ' + policy_text + '\n';
        if (row_counter%2 === 0) {
            table = table + '<tr><td class="plan_cell">' + group_name + '</td><td class="plan_cell" style="text-align:center;">' + policy_text + '</td></tr>\n';
        } else {
            table = table + '<tr class="active"><td class="plan_cell">' + group_name + '</td><td class="plan_cell" style="text-align:center;">' + policy_text + '</td></tr>\n';
        }
    });
    text = text + '</ul>'
    table = table + '<tr><th class="plan_cell">Plan summary<br/><br/>Recommendation statistics</th><th class="plan_cell"><br/><br/>Plans results</th></tr>\n'
    table = table + '<tr><td class="plan_cell">'
    table = table + 'Number of available plans: ' + plan_object["available_plans"] + '<br/>\nNumber of tested plans:' + plan_object["tested_plans"] +  '<br/>\nTime elapsed:' + plan_object["elapsed_time"] + ' s.</td>'
    table = table + '<td class="plan_cell">'
    $.each(plan_object["kpis"], function (index, value) {
        var kpi_unit = '%';
        if (value["value_type"] === "per_unit" ) {
            kpi_unit = '';
        }
        kpi_text = '<b>KPI</b>: ' + KPI_map[value["kpi_name"]] + '<br/>KPI enhancement: ' + (Math.round(parseFloat(value["delta_value"]) * 100) / 100).toString() + kpi_unit + '<br/>Weighted KPI enhancement: ' + (Math.round(parseFloat(value["weighted_delta_value"]) * 100) / 100).toString() + kpi_unit + '<br/>';
        table = table + kpi_text;
    });
    table = table.substring(0, table.length - 5);
    table = table + '</td></tr>\n'
    table = table + '</tbody>\n</table>'
    return table
}

//// Core functions

var gather_and_submit_recommendation_form = function () {
    var validation_result = validate_recommendation_form();
    if (validation_result) {
        var received = false;
        $('#menu_container').hide();
        $('#recommendation-progress').show();
        progress_bar = setInterval(show_progress, 5);

        var url = RECOMMENDATION_URL;
        var message = gather_recommendation_form();
        
        /////// To remove when going live -> ///////
        //DEBUG = true;
        if (DEBUG === true) {    
            $('#JSON_input').html(JSON.stringify(message));
            return false;
        }

        var output = $.ajax ({
            type: 'POST',
            url: url,
            data: JSON.stringify(message), 
            contentType: 'application/json',
            dataType: 'json',
        });
        output.done (function (data) {
            recommendation_object = JSON.parse(data);
            terminate_progress();
            $('#recommendation-progress').hide();
            $('#recommendation-container').show();
            var text = get_plan_html(recommendation_object)
            $('#plan_output').html(text);
        });
        output.fail (function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                "Message: " + xhr.statusText + "\n" +
                "Response: " + xhr.responseText + "\n" + thrownError);
        });
    }
}

function validate_recommendation_form () {
    // validate whether at least one group is selected and whether the total amount of groups to simulate is 100
    var share_total = compute_share();

    // validate whether at least one KPI is selected and extract information about KPI's
    var kpi_is_available = false;
    var kpis = [];
    $('.kpi_checkbox').each(function () {
        if($(this).is(':checked')) {
            var kpi_name = {};
            kpi_name["kpi_name"] = $(this).val();
            kpis.push(kpi_name);
            kpi_is_available = true;
            return false; // exitting the loop
        }
    });

    // validate whether at least one policy is selected
    var policy_is_available = false;
    $('#policy_list input:checked').each(function() {
        policy_is_available = true;
        return false; // exitting the loop
    });
    
    
    // summary
    if (!kpi_is_available) {
        //console.log ('At least one KPI must be selected');
        $('#alert_kpi').css ("display", "block");
        //alert('At least one KPI must be selected');
        return false;
    } else if (!share_total[1]) {
        //console.log ('You must select at least one group');
        $('#alert_share_1').css ("display", "block");
        //alert ('You must select at least one customer group');
        return false;
    } else if (share_total[0] !== 100) {
        //console.log ("Total share must be 100 (" + share_total + ")");
        $('#alert_share_2 > span').html ("Total share <strong>must be 100</strong> (now it is " + share_total[0] + ")");
        $('#alert_share_2').css ("display", "block");
        //alert ("Total share must be 100 (now it is " + share_total[0] + ")");
        return false;
    } else if (!policy_is_available) {
        $('#alert_policies').css ("display", "block");
        return false;
    } else {
        return true;
    }
}

function gather_recommendation_form (url) {    
    // creation of JSON object to sent (JSON_input)
    //console.log ('creation of JSON object');
    var JSON_input = {};
    JSON_input["groups"] = [];
    JSON_input["kpis"] = [];
    JSON_input["policies"] = [];

    // filling groups list
    // - first the groups
    $('.share_slider').each(function () {
        var group_id = '#'+this.id.replace('share_', '')+'_group';
        if ($(group_id).is(':checked')) {
            var value_id = '#' + $(this).attr('data-slider-id') + '_value';
            var group_item = {};
            group_item.name = $(this).attr('id').replace('share_', '');
            group_item.share = parseInt($(value_id).text().replace(' %', ''));
            JSON_input["groups"].push(group_item);
        }
    });
    // - secondly the KPIs
    var service_item = {};
    service_item.service = "video";
    service_item.kpi_names = [];
    $('.kpi_checkbox').each(function () {
        if($(this).is(':checked')) {
            var kpi_name = {};
            kpi_name.name = this.value;
            //console.log(this.value);
            service_item.kpi_names.push(kpi_name);
        }
    });
    JSON_input["kpis"].push(service_item);
    
    // - finally the policies
    var banned_services = [];
    $('#policy_list input:checked').each(function() {
        var policy_item = {};
        policy_item["policy"] = $(this).val()
        if (policy_item["policy"].indexOf("gating") > -1) {
            var parameter = policy_item["policy"].split('-')[1];
            banned_services.push(parameter);
        } else if (policy_item["policy"].indexOf("throttling") > -1) {
            var parameter = policy_item["policy"].split('-')[1];
            policy_item["policy"] = "bandwidth_throttling"
            policy_item["parameters"] = {};
            policy_item["parameters"]["limit"] = parseInt(parameter);
            JSON_input["policies"].push(policy_item);
        } else {
            JSON_input["policies"].push(policy_item);
        }
    });
    var service_combinations = combine(banned_services,1);
    $.each(service_combinations, function (index, value) {
        var policy_item = {};
        policy_item["policy"] = "traffic_gating";
        policy_item["parameters"] = {};
        policy_item["parameters"]["services"] = value;
        JSON_input["policies"].push(policy_item);
    });

    return JSON_input;
}