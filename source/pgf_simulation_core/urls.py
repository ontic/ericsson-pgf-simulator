# -*- coding: utf-8 -*-

###############################################################################
###                                                                         ### 
### COPYRIGHT (c) Ericsson AB 2015                                          ### 
###                                                                         ### 
### The copyright to the computer Program(s) herein is the                  ### 
### property of Ericsson AB, Sweden. The program(s) may be                  ### 
### used and or copied only with the written permission of                  ### 
### Ericsson AB, or in accordance with the terms and conditions             ### 
### stipulated in the agreement contract under which the                    ### 
### program(s) have been supplied.                                          ### 
###                                                                         ### 
###############################################################################

from django.conf.urls import patterns, url
from pgf_simulation_core import views

urlpatterns = patterns('', 
	url(r'^$', views.show_front_end, name='front-end-view'), 
        url(r'^simulations/(?P<simulation_id>[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})$', views.do_simulation, name='simulation_from_file'), 
	url(r'^simulation$|^simulation/$', views.do_simulation, name='root'), 
        url(r'^.*$', views.no_simulation, name='call_with_wrong_id'), 
)
