////////////////////////////////////////////////////////////////////////////////
////                                                                        //// 
//// COPYRIGHT (c) Ericsson AB 2015                                         //// 
////                                                                        //// 
//// The copyright to the computer Program(s) herein is the                 //// 
//// property of Ericsson AB, Sweden. The program(s) may be                 //// 
//// used and or copied only with the written permission of                 //// 
//// Ericsson AB, or in accordance with the terms and conditions            //// 
//// stipulated in the agreement contract under which the                   //// 
//// program(s) have been supplied.                                         //// 
////                                                                        //// 
////////////////////////////////////////////////////////////////////////////////

//// Simulation form validation and submission

//// Simulation configuration
var MAX_PLANS = 4;
var active_plans = 1;
var DEBUG = false;

//// Look and Feel
var LOGO = "ontic";
var VERSION = "0.9";
var FOOTER = "<p>PGF Mitigation Plan Simulation Tool (release <label>" + VERSION + "</label>)<br/> \
                A fine work by <label>Technology and Innovation, Ericsson Spain</label><br/> \
                &copy; LM Ericsson 2015</p>";

// validate_simulation_form takes the chart.html form and makes some basic validation operations.
//If unsuccessful, an alert message is shown:
// - at least one KPI has been selected
// - at least one group is selected and whether the total amount of groups to simulate is 100
// - all plans have a name
// - plan names not duplicated
// - at least one group in each active plan has an assigned policy

var simulation_chart_object = {}; // JavaScript object representation of the JSON simulation output

//// Validation and submission functions

function validate_simulation_form () {
    // validate whether at least one group is selected and whether the total amount of groups to simulate is 100
    //console.log ('validate whether at least one group is selected and whether the total amount of groups to simulate is 100')
    //console.log ('Compute share when submitting')
    var share_total = compute_share();

    // validate whether at least one KPI is selected
    var kpi_checked = false;
    $('.kpi_checkbox').each(function () {
        if($(this).is(':checked')) {
            kpi_checked = true;
            return false; // exit the loop
        }
    });

    if (!kpi_checked) {
        //console.log ('At least one KPI must be selected');
        $('#alert_kpi').css ("display", "block");
        //alert('At least one KPI must be selected');
        return false;
    } else if (!share_total[1]) {
        //console.log ('You must select at least one group');
        $('#alert_share_1').css ("display", "block");
        //alert ('You must select at least one customer group');
        return false;
    } else if (share_total[0] !== 100) {
        //console.log ("Total share must be 100 (" + share_total + ")");
        $('#alert_share_2 > span').html ("Total share <strong>must be 100</strong> (now it is " + share_total[0] + ")");
        $('#alert_share_2').css ("display", "block");
        //alert ("Total share must be 100 (now it is " + share_total[0] + ")");
        return false;
    }

    // plan name validation (not empty, not duplicated)
    //console.log ('plan name validation')
    var plan_names = [];
    var naming_issue = false;
    $('.plan_name').each(function () {
        var plan_id = parseInt($(this).attr('id').replace('plan_name_', ''), 10);
        if (plan_id<=active_plans) {
            var next_div = $(this).parent().siblings('.alert');
            //console.log (next_div.attr('id'));
            if ($(this).val().length === 0) {
                //alert ('Empty plan name. Plans must have a name');
                //console.log('Empty plan name ' + $(this).attr('id'));
                $(this).parent().addClass ("has-error")
                $(this).focus();
                next_div.css( "display", "block" );
                naming_issue = true;
                return false;
            } else {
                $(this).parent().removeClass ("has-error")
                next_div.css( "display", "none" );
                if (plan_names.indexOf($(this).val()) === -1) {
                    plan_names.push($(this).val());
                } else {
                    //console.log ('Duplicated name plan: ' + $(this).attr('id'));
                    alert ('Plan name already used ('+ $(this).attr('id') +')');
                    $(this).focus();
                    //console.log($(this).attr('id'))
                    naming_issue = true;
                    return false;
                }
            }
        }
    });
    if (naming_issue === true) {
        return false;
    }

    // we verify that at least one group in each active plan has an assigned policy
    //console.log ('assigned policy validation')
    var plan_has_policy = false;
    $('.plan_pane').each(function () {
        var plan_id = parseInt($(this).attr('id').replace('plan_', ''), 10);
        //console.log('Analizando plan ' + plan_id);
        //console.log('Just '+active_plans+' plans')
        if (plan_id<=active_plans) {
            plan_has_policy = false;
            //console.log('Analizando plan activo ' + plan_id);
            $(this).find('input[type=checkbox]').each(function () {
                //console.log ($(this).val())
                if ($(this).is(':checked')) {
                    //console.log ('Chequeado: '+$(this).val())
                    var select_id = '#'+$(this).attr('id').replace('_check', '_select');
                    //console.log($(this).attr('id').replace('_check', '_select'));
                    $(select_id).find('option').each(function (i) {
                        //console.log ($(this).val())
                        if ($(this).is(':selected')) {
                            //console.log ('Seleccionado '+$(this).val())
                        }
                        if ($(this).is(':selected') && $(this).val() !== "-1") {
                            //console.log('Cambio de flag');
                            plan_has_policy = true;
                        }
                    });
                }
            });
            if (!plan_has_policy) {
                //alert ('The plan does not include any policy for selected groups')
                $('#alert_plan_'+plan_id).css( "display", "block" );
                console.log('Plan ' + plan_id + ' does not have a policy');
                //console.log($(this).find('input[type=text]').attr('id'))
                //$(this).find('input[type=text]').val('x');
                $(this).find('input[type=text]').focus();
                return false;
            }
        } else {
            return false;
        }
    });
    if (!plan_has_policy) { 
        //console.log ('False');
        return false;
    }
    return true;
}

// compute_share computes the total customer group share
function compute_share () {
    var share_total = 0;
    var is_a_group_selected = false;

    $('.share_table :input').each(function () {
        var group_id = '#'+this.id.replace('share_', '')+'_group';
        if($(group_id).is(':checked')) {
            var value_id = '#' + $(this).attr('data-slider-id') + '_value';
            is_a_group_selected = true;
            share_total = share_total + parseInt($(value_id).text().replace(' %', ''));
            //share_total = share_total + parseInt(this.value);
            //console.log (share_total);
        }
    });
    return [share_total, is_a_group_selected];
}

// set_share_slider takes as argument the total share and shows it in the appropriate slider
function set_share_slider (total_value) {
    //console.log('Total: ' + total_value)
    var x = $("#share_total").slider();
    if (total_value > 100.0) {
        x.slider('setValue', 100);
    } else {
        x.slider('setValue', total_value);
    }
    $(share_slider_total_value).text(total_value + ' %');
    return true;
}

// submit_simulation_form analyzes the simulation form, extracts the information, formats it as a 
//JSON object and POSTs it to url
function submit_simulation_form (url) {    
    // creation of JSON object to sent (JSON_input)
    //console.log ('creation of JSON object');
    var JSON_input = {};
    JSON_input["groups"] = [];
    JSON_input["kpis"] = [];
    JSON_input["plans"] = [];
    JSON_input["source"] = "simulator";

    // filling groups list
    // - first the groups
    $('.share_slider').each(function () {
        var group_id = '#'+this.id.replace('share_', '')+'_group';
        if ($(group_id).is(':checked')) {
            var value_id = '#' + $(this).attr('data-slider-id') + '_value';
            var group_item = {};
            group_item.name = $(this).attr('id').replace('share_', '');
            group_item.share = parseInt($(value_id).text().replace(' %', ''));
            JSON_input["groups"].push(group_item);
        }
    });
    // - secondly the KPIs
    var service_item = {};
    service_item.service = "video";
    service_item.kpi_names = [];
    $('.kpi_checkbox').each(function () {
        if($(this).is(':checked')) {
            var kpi_name = {};
            kpi_name.name = this.value;
            //console.log(this.value);
            service_item.kpi_names.push(kpi_name);
        }
    });
    JSON_input["kpis"].push(service_item);

    // - finally the plans
    $('.form-control[id^="plan_name"]').each(function () {
    //$('.plan_name').each(function () {
        var plan_id = parseInt($(this).attr('id').replace('plan_name_', ''), 10);
        var plan_item = {};
        //console.log ('plan # ' + plan_id + ' (' + active_plans + ')');
        if (plan_id<=active_plans) {
            //console.log ('Analyzing plans');
            plan_item.plan_ref = $(this).val();
            plan_item.policies = [];

            //console.log ("I'm in " + $(this).attr('id'));
            $(this).parent().parent().find("select").each (function (i) {
            //$(this).parent().find("select").each (function (i) {
                //console.log ('The identifier of the select is ' + $(this).attr('id'));
                var policy_tokens = $(this).attr('id').split('_');
                policy_tokens.pop();
                var check_id = policy_tokens.join('_') + '_check';
                //var check_id = $(this).attr('id').replace('_select', '_check');
                //console.log ('The identifier of the check is ' + check_id);
                if ($('#'+check_id).is(':checked')) {
                    //console.log (check_id + ' is checked');
                    $(this).find("option:selected").each (function (i) {
                        var policy_item = {};
                        var subarray = policy_tokens.slice(0, policy_tokens.length-1);
                        policy_item.group = policy_tokens.slice(0, policy_tokens.length-1).join('_');
                        //policy_item.group = policy_tokens[0];
                        if ($(this).val() !== "-1") {
                            policy_item.policy = $(this).val();
                            if (policy_item.policy === 'traffic_gating') {
                                policy_item.parameters = {};
                                policy_item.parameters.services = [];
                            
                                var fieldset_id = policy_tokens.join('_') + '_gating';
                                $('#'+fieldset_id).find('input').each (function () {
                                    if ($(this).is(':checked')) {
                                        policy_item.parameters.services.push($(this).val());
                                    }
                                });
                            } else if (policy_item.policy === 'bandwidth_throttling') {
                                policy_item.parameters = {};
                                policy_item.parameters.limit = 0;
                            
                                var fieldset_id = policy_tokens.join('_') + '_bandwidth';
                                $('#'+fieldset_id).find('input').each (function () {
                                    if ($(this).is(':checked')) {
                                        policy_item.parameters.limit = parseInt($(this).val());
                                    }
                                });
                            }
                            plan_item.policies.push(policy_item);
                        }
                        //console.log ('Option value: ' + $(this).val())
                    });
                }
            });
            JSON_input["plans"].push(plan_item);
        }
    });
    /////// To remove when going live -> ///////
    if (DEBUG === true) {    
        $('#JSON_input').html(JSON.stringify(JSON_input));
    }
    //return JSON_sample;
    /////// -> To remove ///////
    /////// To uncomment -> ///////
    //console.log ('Ajax request starting');
    
    return $.ajax ({
        type: 'POST',
        url: url,
        data: JSON.stringify(JSON_input), 
        contentType: 'application/json',
        dataType: 'json',
    });
    /////// -> To uncomment ///////
}

//// Event handlers

// function to show/hide panes. It handles the click event associated to '#plsel_buttons button'
var show_panes = function () {
    active_plans = parseInt($(this).val(), 10);
    //console.log("Plan selection changed to " + active_plans);
    $('.plan_pane').each(function (index) {
        //console.log(index);
        //console.log($(this).attr('id').replace('plan_', ''));
        var plan_id = parseInt($(this).attr('id').replace('plan_', ''), 10);
        if (plan_id<=active_plans) {
            // show used panes
            $($(this).css("display", "block"));
            //console.log('Panel to show: '+$(this).attr('id'));
        } else {
            // hide unused panes
            $($(this).css("display", "none"));
            //console.log('Panel to hide: '+$(this).attr('id'));
        }
    });
}

// function to handle JSON submission and trigger chart plotting. It handles the click event
//associated to '#submit_button'
var gather_and_submit_simulation_form = function () {
    //console.log("Ready to submit")
    var validation_result = validate_simulation_form();
    if (validation_result) {
        var output = submit_simulation_form(SIMULATION_URL);
        output.done (function (data) {
            simulation_chart_object = JSON.parse(data);
            capture_info (simulation_chart_object);
            $('#control_container').css ('display', 'none');
            draw_tables('main');
            $('#back_button').css ('display', 'block');
        });
        output.fail (function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                "Message: " + xhr.statusText + "\n" +
                "Response: " + xhr.responseText + "\n" + thrownError);
        });
    }
}

//// Auxiliary functions

// function to handle policy selection/deselection related to subscriber groups. It handles the change
//event associated to '.share_checkbox'
var policy_selection = function () {
    var selected_group = $(this).attr('id');
    var selected_checkbox_id;
    var selected_select_id;
    var selected_slider_id = selected_group.replace('group', 'slider');
    var current_total_share = compute_share();
    
    if($(this).is(":checked")) {
        $('[data-slider-id="'+selected_slider_id+'"]').slider("enable");
        //console.log('Checkbox selected');
        //console.log(active_plans.toString());
        for (i = 0; i < MAX_PLANS; i++) {
            // identifiers of checkboxes and select elements for the same group in each group
            selected_checkbox_id = '#' + selected_group.replace('_group', '') + "_"+ (i+1).toString() + "_check";
            selected_select_id = '#' + selected_group.replace('_group', '') + "_"+ (i+1).toString() + "_select";

            // enable this
            // check this
            // enable associated select
            $(selected_checkbox_id).attr('disabled', false);
            $(selected_checkbox_id).prop("checked", true);
            $(selected_select_id).attr('disabled', false);
                  
            // disable remaining checkboxes unless checked
            // disable select elements unless checked
            var selected_checkbox_tokens = selected_checkbox_id.split('_');
            //selected_checkbox_tokens.shift();
            var selected_checkbox_suffix = '_' + selected_checkbox_tokens.slice(-2).join('_');
            //console.log(selected_checkbox_suffix);
            $("input[id$='"+selected_checkbox_suffix+"']").each(function () {
                //console.log($(this).attr('id'));
                if (!($(this).is(":checked"))) {
                    var select_id = $(this).attr('id').replace('check', 'select')
                    $(this).attr("disabled", true);
                    $('#'+select_id).attr("disabled", true);
                }
            });
            //console.log(selected_checkbox_id);
        }
    } else {
        $('[data-slider-id="'+selected_slider_id+'"]').slider("disable");
        for (i = 0; i < MAX_PLANS; i++) { 
            // identifiers of checkboxes and select elements for the same group in each group
            selected_checkbox_id = '#' + selected_group.replace('_group', '') + "_"+ (i+1).toString() + "_check";
            selected_select_id = '#' + selected_group.replace('_group', '') + "_"+ (i+1).toString() + "_select";

            // deselect this
            // disable this
            // disable associated select
            $(selected_checkbox_id).prop("checked", false);
            $(selected_checkbox_id).attr('disabled', true);
            $(selected_select_id).attr('disabled', true);
        }
    }
    set_share_slider (current_total_share[0]);
}

// function to show service gating or bandwidth throttling options when any of such policies is selected
function show_options (option_text, element_id, clone_options) {
    // if this function is called, we need to display proper additional options div (or remove, if the
    //selected options is neither 'bandwidth_throttling' or 'traffic_gating'). 'clone_options' is not
    //relevant
    if (option_text === 'traffic_gating') {
        //console.log('Option recognized (gating)');
        $('#'+element_id+'_gating').css ('display', 'block');
        $('#'+element_id+'_bandwidth').css ('display', 'none');
    } else if (option_text === 'bandwidth_throttling') {
        //console.log('Option recognized (bandwidth)');
        $('#'+element_id+'_gating').css ('display', 'none');
        $('#'+element_id+'_bandwidth').css ('display', 'block');
    } else {
        $('#'+element_id+'_gating').css ('display', 'none');
        $('#'+element_id+'_gating input[type=radio]').prop('checked', false)
        $('#'+element_id+'_bandwidth').css ('display', 'none');
        $('#'+element_id+'_bandwidth input[type=radio]').prop('checked', false)
    }
    // if called for cloning plans, we need to clone also the additional options
    if (clone_options === true) {
        if (option_text === 'traffic_gating') {
            // Procedure to clone additional options if traffic gating is used
            //var selector_checked = '#' + element_id.substring(0, element_id.length-1) + '1_gating' + ' input[type=radio]:checked'
            var selected_value = [];
            $('#' + element_id.substring(0, element_id.length-1) + '1_gating' + ' input[type=radio]:checked').each (function () {
                selected_value.push($(this).val());
            });
            $.each (selected_value, function (index, value) {
                $('#' + element_id + '_gating' + ' input[type=radio][value=' + value + ']').prop('checked', true);
            });
            //console.log (selector_checked);
            //console.log (selected_value);
        } else if (option_text === 'bandwidth_throttling') {
            // Procedure to clone additional options if bandwidth throttling is used
            //var selector_checked = '#' + element_id.substring(0, element_id.length-1) + '1_bandwidth' + ' input[type=radio]:checked'
            var selected_value = $('#' + element_id.substring(0, element_id.length-1) + '1_bandwidth' + ' input[type=radio]:checked').val();
            $('#' + element_id + '_bandwidth' + ' input[type=radio][value=' + selected_value + ']').prop('checked', true);
            //console.log (selector_checked);
            //console.log (selected_value);
        }
        // First, what we do if 
        if (option_text === 'traffic_gating') {
            $('#'+element_id+'_bandwidth input:radio').each(function () {
                //console.log ('#'+element_id+'_gating')
                //console.log ($(this).attr('id'));
                //console.log ($(this).val());
            });
        } else if (option_text === 'bandwidth_throttling') {
            $('#'+element_id+'_bandwidth input:radio').each(function () {
                //console.log ('#'+element_id+'_gating')
                if ($(this).is(':checked')) {
                    //console.log ($(this).attr('id'));
                    //console.log ($(this).val());
                }
            });
        }
    }
}

// function to handle radio buttons
function handle_radio_button (id) {
    //alert ('This is the selected radio button: ' + id + ', with value ' + $('#'+id).val());
    if ($('#'+id).data('was_checked') === undefined) {
        //alert ('First use. Do nothing but updating the flag');
        // $('#'+id).prop('checked', true);
        $('#'+id).data('was_checked', true);
    } else if ($('#'+id).data('was_checked') === true) {
        //alert ('Clicking with checked button -> Uncheck')
        $('#'+id).prop('checked', false);
        $('#'+id).data('was_checked', false);
    } else if ($('#'+id).data('was_checked') === false ) {
        //alert ('Not the first time. Checking again');
        // $('#'+id).prop('checked', true);
        $('#'+id).data('was_checked', true);
    }
}
