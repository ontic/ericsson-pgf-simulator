The **PGF Mitigation Plan Simulator and Recommender** is a Python 2.7 Django 
server that provides simulations of the effects of a given mitigation plan on 
the QoE and, if necessary, recommends the best plan given the parameters of 
the specific QoE degradation situation. It provides a web interface for queries 
from a human operator. RESTful interfaces are provided as well for integration 
from the Policy Governance Function.