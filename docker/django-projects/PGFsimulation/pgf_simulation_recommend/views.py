# -*- coding: utf-8 -*-

###############################################################################
###                                                                         ### 
### COPYRIGHT (c) Ericsson AB 2015                                          ### 
###                                                                         ### 
### The copyright to the computer Program(s) herein is the                  ### 
### property of Ericsson AB, Sweden. The program(s) may be                  ### 
### used and or copied only with the written permission of                  ### 
### Ericsson AB, or in accordance with the terms and conditions             ### 
### stipulated in the agreement contract under which the                    ### 
### program(s) have been supplied.                                          ### 
###                                                                         ### 
###############################################################################

from django.http import HttpResponseServerError, HttpRequest, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

import pgf_recommendation

import json
import socket

# some constants
FE_HTML = 'recommendation.html'
RECOMMENDATION_PATH = 'recommendation/'
DEFAULT_SKIN = 'ericsson'

@csrf_exempt
def show_front_end(request):
    # skin extraction
    SKIN = DEFAULT_SKIN
    logger = pgf_recommendation.get_PGF_logger()
    conf_data = pgf_recommendation.load_conf_data(logger)
    if conf_data["code"] == True :
        SKIN = conf_data["payload"]["general"]["skin"]

    policies = '["'+'","'.join(conf_data["payload"]["policies"])+'"]'
    context = {'path': RECOMMENDATION_PATH, 'skin': SKIN, 'policies': policies }
    
    return render(request, FE_HTML, context)

@csrf_exempt
def get_recommendation(request):
    output_text = u''

#    if request.method == 'POST' or request.body :
    if request.method != 'POST' or not request.body :
#        print ('Regular recommendation request from PGF Mitigation Plan Simulation tool')
#    else :
        print ('Wrong request.')
        return HttpResponseServerError('<b>Server Error</b>:<br/>Wrong result')

    my_result = pgf_recommendation.main(request.body)
    return JsonResponse(my_result, safe=False)
