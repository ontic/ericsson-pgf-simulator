from django.conf.urls import patterns, url
from pgf_simulation_recommend import views

urlpatterns = patterns('', 
                       url(r'^$', views.show_front_end, name='front-end-view'), 
                       url(r'^recommendation$|^recommendation/$', views.get_recommendation, name='recommend_result'), 
)
