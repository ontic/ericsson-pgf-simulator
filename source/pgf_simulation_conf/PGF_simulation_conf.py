# -*- coding: utf-8 -*-

###############################################################################
###                                                                         ### 
### COPYRIGHT (c) Ericsson AB 2015                                          ### 
###                                                                         ### 
### The copyright to the computer Program(s) herein is the                  ### 
### property of Ericsson AB, Sweden. The program(s) may be                  ### 
### used and or copied only with the written permission of                  ### 
### Ericsson AB, or in accordance with the terms and conditions             ### 
### stipulated in the agreement contract under which the                    ### 
### program(s) have been supplied.                                          ### 
###                                                                         ### 
###############################################################################

"""
PGF simulation configuration tools

This file provides all the functions needed to validate the inputs to PGF_simulation.py

The following functions are provided:
- validate_config: it validates the configuration file. The configuration file follows
the INI format. Read of configuration file is based on ConfigParser. The configuration
file contents are returned as a dictionary
- get_example_JSON: it reads the JSON input. It is currently based on a simple JSON example
read from a file (input_JSON_example). It returns the JSON input as a dictionary.
- validate_JSON_input: it validates the JSON input. First, a JSON schema validation is
performed. Next, its contents are validated according to what is supported in the 
configuration. A dictionary with what has to be simulated is returned

TODO:
- turn configuration file into a JSON structure

"""
import ConfigParser
from datetime import datetime
import json
from jsonschema import validate
import logging
import os.path
import time
import types

#from PGF_simulation_paths import base_file_path
base_file_path = os.path.dirname(os.path.realpath(__file__))
conf_file_name = "aqoe.config"  # configuration file name
conf_file      = os.path.join(base_file_path, conf_file_name)
log_file_name  = time.strftime("%Y%m%d") + "_PGF_simulation.log"
log_file       = os.path.join(base_file_path, 'log', log_file_name)

# control lists used to verify that the configuration and the inputs are right
control              = dict()
control["sections"]  = ['General', 'Kpi', 'Weights']                                # Mandatory sections in 
                                                                                    #the configuration file
control["general"]   = ['max_users', 'kpi_gain_function', 'policies', 'services', 'kpi', 'ratio_type', 'max_throttling', 'min_throttling']
                                                                                    # Mandatory items in 
                                                                                    #[General] section
control["functions"] = ['linear', 'exponentiation', 'logistic']                     # Supported KPI computation
                                                                                    #functions
control["policies"]  = ['wifi', 'to3g', 'video_acceleration', 'bandwidth_throttling', 'traffic_gating']
                                                                                    # Types of mitigation 
                                                                                    #policies
control["services"]  = ['video' , 'file_transfer', 'web_browsing']                  # Supported services
control["kpi"]       = [('vfr', 'video_freeze_rate'),
                        ('va', 'video_accessibility')]                              # Supported KPI's

# logging global variable
loggers = False

# JSON inputs
input_JSON_name = "simulator_input.json"
admin_JSON_name = "admin_input.json"
recommend_JSON_name = "recommendator_input.json"

input_JSON_file = os.path.join(base_file_path, input_JSON_name)
admin_JSON_file = os.path.join(base_file_path, admin_JSON_name)
recommend_JSON_file = os.path.join(base_file_path, recommend_JSON_name)

# Available policies
default_policies = [
    { "policy": "video_acceleration" }, 
    { "policy": "bandwidth_throttling", "parameters": { "limit": 3000 } }, 
    { "policy": "bandwidth_throttling", "parameters": { "limit": 1000 } }, 
    { "policy": "bandwidth_throttling", "parameters": { "limit": 64 } },
    { "policy": "wifi" }, 
    { "policy": "to3g" }, 
    { "policy": "traffic_gating", "parameters": { "services": set([ "file_transfer" ]) } }, 
    { "policy": "traffic_gating", "parameters": { "services": set([ "web_browsing" ]) } }, 
    { "policy": "traffic_gating", "parameters": { "services": set([ "video" ]) } }, 
    { "policy": "traffic_gating", "parameters": { "services": set([ "web_browsing", "file_transfer" ]) } }, 
    { "policy": "traffic_gating", "parameters": { "services": set([ "video", "file_transfer" ]) } }, 
    { "policy": "traffic_gating", "parameters": { "services": set([ "web_browsing", "video" ]) } }, 
    { "policy": "traffic_gating", "parameters": { "services": set([ "video", "web_browsing", "file_transfer" ]) } }
]

# Policy constraints
constraint_matrix = dict()
constraint_matrix["gold"]             = [True, True, False, False, True, True, False, False, False, False, False, False, False, True]
constraint_matrix["silver_corporate"] = [True, True, True, False, True, True, False, False, False, False, False, False, False, True]
constraint_matrix["silver_domestic"]  = [True, True, True, False, True, True, True, False, False, False, False, False, False, True]
constraint_matrix["bronze_domestic"]  = [True, True, True, True, True, True, True, True, True, True, True, True, True, True]
constraint_matrix["bronze_corporate"] = [True, True, True, False, True, True, True, True, False, True, False, False, False, True]
constraint_matrix["bronze_young"]     = [True, True, True, True, True, True, True, True, True, True, True, True, True, True]

'''contraint_matrix2 = [False] * 14
for index, value in enumerate(constraints["gold"]) :
    if value : contraint_matrix2[index] = value'''

################################################################
# Logging functions
################################################################
def get_PGF_logger (logging_level='DEBUG') :
    global loggers 
    
    logger = logging.getLogger(__name__)
   
    if loggers == False :
        if logging_level == 'DEBUG' :
            logger.setLevel(logging.DEBUG)
        else :
            logger.setLevel(logging.WARNING)

        formatter = logging.Formatter('%(levelname)s %(message)s')

        ch = logging.StreamHandler()
        ch.setLevel(logging.WARNING)
        ch.setFormatter(formatter)
        logger.addHandler(ch)
        
        try :
            fh = logging.FileHandler(log_file)
            if logging_level == 'DEBUG' :
                fh.setLevel(logging.DEBUG)
            else :
                fh.setLevel(logging.WARNING)
            fh.setFormatter(formatter)
            logger.addHandler(fh)
        except :
            print 'Unable to create log file'
            logger.error('[Creation of log file %s (%s) not possible]' % (log_file, datetime.now().strftime('%Y-%m-%d %H:%M:%S')))

        logger.info('[Creation of log file %s (%s)]' % (log_file, datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        loggers = True
    return logger

################################################################
# Validation functions
################################################################
def validate_config (logger) :
    # the configuration file contents are stored as a dict-based python structure (conf_container)
    conf_container = dict()                     # configuration information. Returned by the function
    conf_container["policies"]       = list()   # list of supported policies (from configuration file)
    conf_container["services"]       = dict()   # dictionary of traffic share, per type of service (to
                                                #apply traffic gating) (from configuration file)
    conf_container["general"]        = dict()   # dictionary with generic parameters (parameters for 
                                                #functions, andsf rate...) (from configuration file)
    conf_container["weights"]        = dict()   # dictionary with the weights of every supported 
                                                #subscriber group (from configuration file)
    conf_container["supported_kpis"] = dict()   # KPI's from configuration file with respective 
                                                #thresholds and associated values
    conf_container["policy_limits"]  = dict()   # Upper boundaries for the application of policies
                                                #(expressed as percentages)
    
    try:
        #print 'Processing of configuration file starts'
        config = ConfigParser.SafeConfigParser()
        config.read(conf_file)
        
        # Verification of mandatory sections
        for section_name in control["sections"] :
            if section_name not in config.sections() : raise ValueError('Mandatory section ['+section_name+'] not found')
            
        # Verification of mandatory options in [General] section
        for item_name in control["general"] :
            if item_name not in config.options('General') : raise ValueError ('Mandatory item ('+item_name+') in section [General] is not detected')

        conf_container["general"]['max_users'] = config.get('General', 'max_users')
        conf_container["general"]['max_throttling'] = int(config.get('General', 'max_throttling'))
        conf_container["general"]['min_throttling'] = int(config.get('General', 'min_throttling'))

        if config.get('General', 'ratio_type') == 'one' : conf_container["general"] ['ratio_factor'] = 1.0
        elif config.get('General', 'ratio_type') == 'hundred' : conf_container["general"] ['ratio_factor'] = 100.0
        else : raise ValueError ('Ratio factor in section [General] is not valid (must be "one" or "hundred")')
    
        if config.get('General', 'individual_kpi_ratio') == 'one' : conf_container["general"] ['individual_kpi_ratio'] = "per_one"
        elif config.get('General', 'individual_kpi_ratio') == 'hundred' : conf_container["general"] ['individual_kpi_ratio'] = "per_cent"
        else : raise ValueError ('Individual KPI ratio factor in section [General] is not valid (must be "one" or "hundred")')

        if config.get('General', 'global_kpi_ratio') == 'one' : conf_container["general"] ['global_kpi_ratio'] = "per_one"
        elif config.get('General', 'global_kpi_ratio') == 'hundred' : conf_container["general"] ['global_kpi_ratio'] = "per_cent"
        else : raise ValueError ('Global KPI ratio factor in section [General] is not valid (must be "one" or "hundred")')

        # Verification of policies and creation of policy list (those required to be supported according to configuration)
        policies = config.get('General', 'policies')
        for policy_name in policies.split(',') :
            policy_name = policy_name.strip()
            if policy_name not in control["policies"] :
                raise ValueError ('Policy name ('+policy_name+') not valid')
            else :
                conf_container["policies"].append(policy_name)
        
        # Verification of services and creation of services list
        services = config.get('General', 'services')
        for service_name in services.split(',') :
            service_name = service_name.strip()
            if service_name not in control["services"] :
                raise ValueError ('Service name ('+service_name+') not valid')
            else :
                conf_container["services"][service_name] = None

        # Verification of KPI gain function and filling of configuration dictionary
        #print 'Verification of KPI gain function in configuration file and filling of configuration dictionary'
        conf_container["general"] ['kpi_gain_function'] = config.get('General', 'kpi_gain_function')
        if conf_container["general"] ['kpi_gain_function'] not in control["functions"] : raise ValueError ('KPI gain function type is not valid')
        if ((not config.has_option('General', 'logistic_parameter')) or (not config.has_option('General', 'exponentiation_parameter'))) :
            raise ValueError ('Parameters for modelling functions not available')

        conf_container["general"] ['kpi_gain_parameter'] = config.get('General', 'logistic_parameter')
        conf_container["general"] ['kpi_gain_parameter_2'] = config.get('General', 'exponentiation_parameter')
        
        # Extraction of skin parameter
        if 'skin' not in config.options('General') :
            conf_container["general"] ['skin'] = 'ericsson'
        else :
            conf_container["general"] ['skin'] = config.get('General', 'skin')
        
        # Verification of availability of parameters associated to each policy and creation of services dictionary
        #print 'Verification of availability of parameters associated to each policy in configuration file and filling of services dictionary'
        if 'wifi' in conf_container["policies"] :
            if not config.has_section('Wifi'): raise ValueError('Mandatory section [Wifi] not found')
            elif not config.has_option('Wifi', 'andsf_ratio'): raise ValueError('Mandatory option in [Wifi] ("andsf_ratio") not found') 
            else : conf_container["general"]['andsf_ratio'] = config.get('Wifi', 'andsf_ratio')
            
        if 'traffic_gating' in conf_container["policies"] :
            if not config.has_section('Traffic Gating'): raise ValueError('Mandatory section [Traffic Gating] not found')
            
            if set(config.options('Traffic Gating')) == set(conf_container["services"]) : 
                for name, value in config.items('Traffic Gating') :
                    conf_container["services"][name] = value
                if sum(float(i) for i in conf_container["services"].values()) != 100.0 : raise ValueError('Total service traffic mix must be 100. Now it is '+str(sum(float(i) for i in conf_container["services"].values())))
            else :
                raise ValueError('List of services in [Traffic Gating] not valid')
            
        if 'video_acceleration' in conf_container["policies"] :
            if not config.has_section('Video Acceleration'): raise ValueError('Mandatory section [Video Acceleration] not found')
            elif not config.has_option('Video Acceleration', 'saving_ratio'): raise ValueError('Mandatory option in [Video Acceleration] ("saving_ratio") not found') 
            else : conf_container["general"]['video_acc_saving_ratio'] = config.get('Video Acceleration', 'saving_ratio')
    
        # Creation of weights dictionary
        #print 'Filling of subscriber group weights dictionary'
        for name, value in config.items ('Weights') :
            conf_container["weights"][name] = value
    
        # Verification and filling of KPI thresholds
        #print 'Verification of KPI information in configuration file and filling of KPI thresholds'
        kpis = config.get('General', 'kpi')
        for kpi_name in kpis.split(',') :
            kpi_name = kpi_name.strip()
            if not dict(control["kpi"]).has_key(kpi_name) :
                raise ValueError ('KPI name ('+kpi_name+') not valid')
            else :
                conf_container["supported_kpis"][kpi_name] = dict()
                if (config.has_option('Kpi', 'threshold_'+kpi_name+'_kpi') and config.has_option('Kpi', 'best_'+kpi_name+'_kpi') and config.has_option('Kpi', 'worst_'+kpi_name+'_kpi')) :
                    conf_container["supported_kpis"][kpi_name]["threshold"] = config.get('Kpi', 'threshold_'+kpi_name+'_kpi')
                    conf_container["supported_kpis"][kpi_name]["best"] = config.get('Kpi', 'best_'+kpi_name+'_kpi')
                    conf_container["supported_kpis"][kpi_name]["worst"] = config.get('Kpi', 'worst_'+kpi_name+'_kpi')
                else : raise ValueError('KPI value(s) for '+kpi_name+' not found')
                if (config.has_option('Kpi', 'to3g_'+kpi_name+'_kpi')) :
                    conf_container["supported_kpis"][kpi_name]["to3g"] = config.get('Kpi', 'to3g_'+kpi_name+'_kpi')
                if config.has_section('Wifi') :
                    argument = 'wifi_'+kpi_name+'_kpi'
                    if config.has_option('Kpi', argument): conf_container["supported_kpis"][kpi_name]["wifi"] = config.get('Kpi', 'wifi_'+kpi_name+'_kpi')
                    else: raise ValueError('KPI value for '+kpi_name+' (Offload to Wifi) not found')

        # Policy constraints extraction (limits)
        conf_container["policy_limits"] = {"to3g": 100.0, "wifi": 100.0, "video_acceleration": 100.0}
        if 'wifi' in conf_container["policies"] :
            if (config.has_option('Total-Constraints', 'wifi')) :
                conf_container["policy_limits"]["wifi"] = config.get('Total-Constraints', 'wifi')
            else :
                conf_container["policy_limits"].pop("wifi", None)
        if 'to3g' in conf_container["policies"] :
            if (config.has_option('Total-Constraints', 'to3g')) :
                conf_container["policy_limits"]["to3g"] = config.get('Total-Constraints', 'to3g')
            else :
                conf_container["policy_limits"].pop("to3g", None)
        if 'video_acceleration' in conf_container["policies"] :
            if (config.has_option('Total-Constraints', 'video_acceleration')) :
                conf_container["policy_limits"]["video_acceleration"] = config.get('Total-Constraints', 'video_acceleration')
            else :
                conf_container["policy_limits"].pop("video_acceleration", None)

        # Policy constraints extraction (valid policies)
        for group_name in conf_container["weights"] :
            if (config.has_option('Policy-Constraints', group_name)) :
                constraint_matrix[group_name] = [True] * 14
                raw_policies = config.get('Policy-Constraints', group_name).replace(' ', '').split(',')
                for raw_policy in raw_policies :
                    sample_policy = dict()
                    sample_policy["parameters"] = dict()
                    raw_policy_items = raw_policy.split('-')
                    sample_policy["policy"] = raw_policy_items[0]
                    #print raw_policy_items[0]
                    if raw_policy_items[0] == 'traffic_gating' :
                        sample_policy["parameters"]["services"] = set(raw_policy_items[1].split('|'))
                        #print raw_policy_items[1]
                    elif raw_policy_items[0] == 'bandwidth_throttling' :
                        sample_policy["parameters"]["limit"] = int(raw_policy_items[1])
                        #print raw_policy_items[1]

                    if sample_policy in default_policies :
                        constraint_matrix[group_name][default_policies.index(sample_policy)] = False
                        #print default_policies.index(sample_policy)

        not_supported_policies = list(set(control["policies"]) - set(conf_container["policies"]))
        for (index, value) in enumerate(default_policies) :
            if value["policy"] in not_supported_policies :
                for constraint in constraint_matrix :
                    constraint_matrix[constraint][index] = False
                
        #for not_supported_policy in not_supported_policies :
        #    for (index, value) in enumerate(default_policies) :
        #        if value["policy"] == not_supported_policy : 

        print 'Configuration file processing ends'
        
    except (ConfigParser.NoSectionError, ConfigParser.NoOptionError, ConfigParser.Error) :
        logger.error('Configuration file error')
        raise NameError('Configuration error')
    except ValueError as error :
        logger.error(error)
        raise NameError(error)
    else :
        return conf_container

def validate_JSON_schema (JSON_data, JSON_schema_type, logger) :
    if JSON_schema_type == 'simulation' :
        with open(input_JSON_file, 'r') as content_file:
            JSON_schema = json.loads(content_file.read())
    elif JSON_schema_type == 'admin' :
        with open(admin_JSON_file, 'r') as content_file:
            JSON_schema = json.loads(content_file.read())
    elif JSON_schema_type == 'recommend' :
        with open(recommend_JSON_file, 'r') as content_file:
            JSON_schema = json.loads(content_file.read())

    #JSON file validation
    try :
        #print 'Validation of input JSON file'
        validate(JSON_data, JSON_schema)
        #print json.dumps(schema,indent=1)
    except Exception as error:
        logger.error(error)
        raise NameError(error)
    return True

def validate_JSON_input (JSON_data, configuration, logger) :
    #JSON file validation
    try :
        #print 'Validation of input JSON file'
        validate(JSON_data, input_JSON_schema)
    #    print json.dumps(schema,indent=1)
    except Exception as error:
        logger.error(error)
        raise NameError(error)
    
    # inputs obtained from the JSON file
    input_contents = dict ()
    input_contents["shares"] = dict()     # dictionary of subscriber groups provided as input to the simulation. Key is the group name; value is the group share
    input_contents["services"] = dict()   # dictionary of services and corresponding kpis to simulate. Key is the service name; value is a list of kpi names
    input_contents["plans"] = dict()      # dictionary of plans to simulate after postprocessing. Key is the plan reference; value is a dictionary containing the groups, the policies and the parameters
    
    try :
        # Verification of groups provided in JSON being also in configuration file. Group information is stored
        #print 'Verification of subscriber groups from input JSON file'
        for subscriber_group in JSON_data["groups"] :
            if configuration["weights"].has_key(subscriber_group["name"]) :
                input_contents["shares"][subscriber_group["name"]] = float(subscriber_group["share"])
            else :
                raise ValueError ('Subscriber group requested in simulation ('+subscriber_group["name"]+') not supported')
    
        # Verification of kpis provided in JSON file also in configuration file. KPI information is stored as a dictionary of lists
        #print 'Verification of requested KPIs in input JSON file'
        for kpi in JSON_data["kpis"] :
            if not configuration["services"].has_key(kpi["service"]) : 
                raise ValueError ('Service type requested in simulation ('+kpi["service"]+') not supported')
            for kpi_name in kpi["kpi_names"] :
                if kpi_name["name"] not in dict(control["kpi"]).values() :
                    raise ValueError ('KPI type requested in simulation ('+kpi_name["name"]+') not supported')
                input_contents["services"].setdefault(kpi["service"], []).append(kpi_name["name"])

        # Verification of mitigation plans to simulate provided in JSON. KPI information is stored as a dictionary of lists
        #print 'Verification of mitigation plans in input JSON file'
        for plan in JSON_data["plans"] :
            for item in plan["policies"] :
                if item["policy"] not in control["policies"] and item["policy"] != "no_policy": raise ValueError ('Policy type ('+item["policy"]+') not supported')
                if item["policy"] == u'bandwidth_throttling' :
                    if (
                        ('parameters' not in item) or ('limit' not in item["parameters"]) or 
                        (item["parameters"]["limit"] > configuration["general"]["max_throttling"]) or
                        (item["parameters"]["limit"] < configuration["general"]["min_throttling"])
                        ) :
                        raise ValueError ('Bandwidth throttling parameters not present or beyond boundaries')
                if item["policy"] == u'traffic_gating' :
                    if (
                        ('parameters' not in item) or ('services' not in item["parameters"]) or 
                        (len(item["parameters"]["services"]) == 0) or 
                        (set(item["parameters"]["services"]).issubset(set(configuration["services"].keys())) == False)
                        ) :
                        raise ValueError ('Traffic gating parameters not present or not following naming conventions')
                if not configuration["weights"].has_key(item["group"]) and (item["group"] != 'default') :
                    raise ValueError ('Subscriber group ('+item["group"]+') not supported')

                input_contents["plans"].setdefault(plan["plan_ref"], []).append(item)
    
        # Determination of subscriber groups under 'default' label (default_groups list) in the plan
        for plan_name in input_contents["plans"] :
            # Determination of groups under 'default' label
            default_groups = list()
            for group in input_contents["shares"].keys() :
                default_groups.append(group)
            for item in input_contents["plans"][plan_name] :
                if item["group"] != 'default' :
                    default_groups.remove(item["group"])
    
            # Transformation of 'default' policies into policies applicable to real groups
            temp_array_policy = list()
            for policy in input_contents["plans"][plan_name] :
    #            print policy
                if policy["group"] == "default" :
                    temp_policy = dict()
                    for group in default_groups :
                        temp_policy = policy.copy ()
                        temp_policy["group"] = group
                        temp_array_policy.append(temp_policy)
                        
                    input_contents["plans"][plan_name].remove(policy)
            for item in temp_array_policy :
                input_contents["plans"][plan_name].append(item)
    
    except ValueError as error :
        logger.error(error)
        raise NameError(error)
    else :
        #print 'Verification of input JSON file ends'
        return input_contents

def validate_JSON_admin_input (JSON_data, configuration, logger) :
    #print 'Validation of configuration file'
    conf_data = validate_config (logger)
    #print 'Validation of configuration file result: correct'

    #print ('Starting set operation')

    try:
        config = ConfigParser.ConfigParser()
        config.read(conf_file)
        if "kpi_gain_function" in JSON_data and len(JSON_data["kpi_gain_function"]) > 0 :
            config.set('General', "kpi_gain_function", JSON_data["kpi_gain_function"][0].lower() + JSON_data["kpi_gain_function"][1:])
        
        if "user_share_ratio" in JSON_data and len(JSON_data["user_share_ratio"]) > 0 :
            if JSON_data["user_share_ratio"] == 'Percentage' :
                config.set('General', "ratio_type", 'hundred')
            elif JSON_data["user_share_ratio"] == 'Unit' :
                config.set('General', "ratio_type", 'one')
        if "individual_kpi_ratio" in JSON_data and len(JSON_data["individual_kpi_ratio"]) > 0 :
            if JSON_data["individual_kpi_ratio"] == 'Percentage' :
                config.set('General', "individual_kpi_ratio", 'hundred')
            elif JSON_data["individual_kpi_ratio"] == 'Unit' :
                config.set('General', "individual_kpi_ratio", 'one')
        if "global_kpi_ratio" in JSON_data and len(JSON_data["global_kpi_ratio"]) > 0 :
            if JSON_data["global_kpi_ratio"] == 'Percentage' :
                config.set('General', "global_kpi_ratio", 'hundred')
            elif JSON_data["global_kpi_ratio"] == 'Unit' :
                config.set('General', "global_kpi_ratio", 'one')

        share_ratio = config.get ('General', "ratio_type")
        if "andsf_ratio" in JSON_data and is_number(JSON_data["andsf_ratio"]) and is_in_range (JSON_data["andsf_ratio"], share_ratio) :
            config.set('Wifi', "andsf_ratio", JSON_data["andsf_ratio"])

        if "video_acceleration_ratio" in JSON_data and is_number(JSON_data["video_acceleration_ratio"]) and is_in_range (JSON_data["video_acceleration_ratio"], share_ratio) :
            config.set('Video Acceleration', "saving_ratio", JSON_data["video_acceleration_ratio"])

        if "video" in JSON_data["services"] and is_number(JSON_data["services"]["video"]) and is_in_range (JSON_data["services"]["video"], share_ratio) :
            video_share = config.get('Traffic Gating', "video")
            config.set('Traffic Gating', "video", JSON_data["services"]["video"])

        if "file_transfer" in JSON_data["services"] and is_number(JSON_data["services"]["file_transfer"]) and is_in_range (JSON_data["services"]["file_transfer"], share_ratio) :
            file_transfer_share = config.get('Traffic Gating', "file_transfer")
            config.set('Traffic Gating', "file_transfer", JSON_data["services"]["file_transfer"])

        if "web_browsing" in JSON_data["services"] and is_number(JSON_data["services"]["web_browsing"]) and is_in_range (JSON_data["services"]["web_browsing"], share_ratio) :
            web_browsing_share = config.get('Traffic Gating', "web_browsing")
            config.set('Traffic Gating', "web_browsing", JSON_data["services"]["web_browsing"])
        
        if (JSON_data["services"]["video"] + JSON_data["services"]["file_transfer"] + JSON_data["services"]["web_browsing"]) != 100 :
            config.set('Traffic Gating', "video", video_share)
            config.set('Traffic Gating', "file_transfer", file_transfer_share)
            config.set('Traffic Gating', "web_browsing", web_browsing_share)
            
        if "vfr" in JSON_data["kpis"] :
            if "wifi"  in JSON_data["kpis"]["vfr"] and is_number(JSON_data["kpis"]["vfr"]["wifi"]) :
                config.set('Kpi', "wifi_vfr_kpi", JSON_data["kpis"]["vfr"]["wifi"])
            if "to3g"  in JSON_data["kpis"]["vfr"] and is_number(JSON_data["kpis"]["vfr"]["to3g"]) :
                config.set('Kpi', "to3g_vfr_kpi", JSON_data["kpis"]["vfr"]["to3g"])

        if "va" in JSON_data["kpis"] :
            if "wifi"  in JSON_data["kpis"]["va"] and is_number(JSON_data["kpis"]["va"]["wifi"]) :
                config.set('Kpi', "wifi_va_kpi", JSON_data["kpis"]["va"]["wifi"])
            if "to3g"  in JSON_data["kpis"]["va"] and is_number(JSON_data["kpis"]["va"]["to3g"]) :
                config.set('Kpi', "to3g_va_kpi", JSON_data["kpis"]["va"]["to3g"])

        if "gold" in JSON_data["groups"] and is_number(JSON_data["groups"]["gold"]) :
            config.set('Weights', "gold", JSON_data["groups"]["gold"])
        if "silver_domestic" in JSON_data["groups"] and is_number(JSON_data["groups"]["silver_domestic"]) :
            config.set('Weights', "silver_domestic", JSON_data["groups"]["silver_domestic"])
        if "silver_corporate" in JSON_data["groups"] and is_number(JSON_data["groups"]["silver_corporate"]) :
            config.set('Weights', "silver_corporate", JSON_data["groups"]["silver_corporate"])
        if "bronze_domestic" in JSON_data["groups"] and is_number(JSON_data["groups"]["bronze_domestic"]) :
            config.set('Weights', "bronze_domestic", JSON_data["groups"]["bronze_domestic"])
        if "bronze_corporate" in JSON_data["groups"] and is_number(JSON_data["groups"]["bronze_corporate"]) :
            config.set('Weights', "bronze_corporate", JSON_data["groups"]["bronze_corporate"])
        if "gold" in JSON_data["groups"] and is_number(JSON_data["groups"]["gold"]) :
            config.set('Weights', "gold", JSON_data["groups"]["gold"])
        
        with open(conf_file, 'wb') as cf:
            config.write(cf)
        cf.close();

    except (ConfigParser.NoSectionError, ConfigParser.NoOptionError, ConfigParser.Error) :
        logger.error('Configuration file error')
        raise NameError('Configuration error')

    #print ('Ending set operation')
    return True


def validate_JSON_input2 (JSON_data, configuration, input_type, logger) :
    if input_type == 'simulation' :
        # Validation of regular simulation file
        # Schema validation
        '''
        try :
            #print 'Validation of input JSON file'
            validate(JSON_data, input_JSON_schema)
        #    print json.dumps(schema,indent=1)
        except Exception as error:
            logger.error(error)
            raise NameError(error)
        '''
        # inputs obtained from the JSON file
        input_contents = dict ()
        input_contents["shares"] = dict()     # dictionary of subscriber groups provided as input to the simulation. Key is the group name; value is the group share
        input_contents["services"] = dict()   # dictionary of services and corresponding kpis to simulate. Key is the service name; value is a list of kpi names
        input_contents["plans"] = dict()      # dictionary of plans to simulate after postprocessing. Key is the plan reference; value is a dictionary containing the groups, the policies and the parameters
        
        # Semantics validation
        try :
            # Verification of groups provided in JSON being also in configuration file. Group information is stored
            #print 'Verification of subscriber groups from input JSON file'
            for subscriber_group in JSON_data["groups"] :
                if configuration["weights"].has_key(subscriber_group["name"]) :
                    input_contents["shares"][subscriber_group["name"]] = float(subscriber_group["share"])
                else :
                    raise ValueError ('Subscriber group requested in simulation ('+subscriber_group["name"]+') not supported')
        
            # Verification of kpis provided in JSON file also in configuration file. KPI information is stored as a dictionary of lists
            #print 'Verification of requested KPIs in input JSON file'
            for kpi in JSON_data["kpis"] :
                if not configuration["services"].has_key(kpi["service"]) : 
                    raise ValueError ('Service type requested in simulation ('+kpi["service"]+') not supported')
                for kpi_name in kpi["kpi_names"] :
                    if kpi_name["name"] not in dict(control["kpi"]).values() :
                        raise ValueError ('KPI type requested in simulation ('+kpi_name["name"]+') not supported')
                    input_contents["services"].setdefault(kpi["service"], []).append(kpi_name["name"])
    
            # Verification of mitigation plans to simulate provided in JSON. KPI information is stored as a dictionary of lists
            #print 'Verification of mitigation plans in input JSON file'
            for plan in JSON_data["plans"] :
                for item in plan["policies"] :
                    if item["policy"] not in control["policies"] and item["policy"] != "no_policy": raise ValueError ('Policy type ('+item["policy"]+') not supported')
                    if item["policy"] == u'bandwidth_throttling' :
                        if (
                            ('parameters' not in item) or ('limit' not in item["parameters"]) or 
                            (item["parameters"]["limit"] > configuration["general"]["max_throttling"]) or
                            (item["parameters"]["limit"] < configuration["general"]["min_throttling"])
                            ) :
                            raise ValueError ('Bandwidth throttling parameters not present or beyond boundaries')
                    if item["policy"] == u'traffic_gating' :
                        if (
                            ('parameters' not in item) or ('services' not in item["parameters"]) or 
                            (len(item["parameters"]["services"]) == 0) or 
                            (set(item["parameters"]["services"]).issubset(set(configuration["services"].keys())) == False)
                            ) :
                            raise ValueError ('Traffic gating parameters not present or not following naming conventions')
                    if not configuration["weights"].has_key(item["group"]) and (item["group"] != 'default') :
                        raise ValueError ('Subscriber group ('+item["group"]+') not supported')
    
                    input_contents["plans"].setdefault(plan["plan_ref"], []).append(item)
        
            # Determination of subscriber groups under 'default' label (default_groups list) in the plan
            for plan_name in input_contents["plans"] :
                # Determination of groups under 'default' label
                default_groups = list()
                for group in input_contents["shares"].keys() :
                    default_groups.append(group)
                for item in input_contents["plans"][plan_name] :
                    if item["group"] != 'default' :
                        default_groups.remove(item["group"])
        
                # Transformation of 'default' policies into policies applicable to real groups
                temp_array_policy = list()
                for policy in input_contents["plans"][plan_name] :
        #            print policy
                    if policy["group"] == "default" :
                        temp_policy = dict()
                        for group in default_groups :
                            temp_policy = policy.copy ()
                            temp_policy["group"] = group
                            temp_array_policy.append(temp_policy)
                            
                        input_contents["plans"][plan_name].remove(policy)
                for item in temp_array_policy :
                    input_contents["plans"][plan_name].append(item)
        
        except ValueError as error :
            logger.error(error)
            raise NameError(error)
        else :
            #print 'Verification of input JSON file ends'
            return input_contents
    elif input_type == 'admin' :
        #print 'Validation of configuration file'
        conf_data = validate_config (logger)
        #print 'Validation of configuration file result: correct'
    
        #print ('Starting set operation')
    
        try:
            config = ConfigParser.ConfigParser()
            config.read(conf_file)
            if "skin" in JSON_data and len(JSON_data["skin"]) > 0 :
                config.set('General', "skin", JSON_data["skin"])
            
            if "kpi_gain_function" in JSON_data and len(JSON_data["kpi_gain_function"]) > 0 :
                config.set('General', "kpi_gain_function", JSON_data["kpi_gain_function"][0].lower() + JSON_data["kpi_gain_function"][1:])
                
            if "user_share_ratio" in JSON_data and len(JSON_data["user_share_ratio"]) > 0 :
                if JSON_data["user_share_ratio"] == 'Percentage' :
                    config.set('General', "ratio_type", 'hundred')
                elif JSON_data["user_share_ratio"] == 'Unit' :
                    config.set('General', "ratio_type", 'one')
            if "individual_kpi_ratio" in JSON_data and len(JSON_data["individual_kpi_ratio"]) > 0 :
                if JSON_data["individual_kpi_ratio"] == 'Percentage' :
                    config.set('General', "individual_kpi_ratio", 'hundred')
                elif JSON_data["individual_kpi_ratio"] == 'Unit' :
                    config.set('General', "individual_kpi_ratio", 'one')
            if "global_kpi_ratio" in JSON_data and len(JSON_data["global_kpi_ratio"]) > 0 :
                if JSON_data["global_kpi_ratio"] == 'Percentage' :
                    config.set('General', "global_kpi_ratio", 'hundred')
                elif JSON_data["global_kpi_ratio"] == 'Unit' :
                    config.set('General', "global_kpi_ratio", 'one')
    
            share_ratio = config.get ('General', "ratio_type")
            if "andsf_ratio" in JSON_data and is_number(JSON_data["andsf_ratio"]) and is_in_range (JSON_data["andsf_ratio"], share_ratio) :
                config.set('Wifi', "andsf_ratio", JSON_data["andsf_ratio"])
    
            if "video_acceleration_ratio" in JSON_data and is_number(JSON_data["video_acceleration_ratio"]) and is_in_range (JSON_data["video_acceleration_ratio"], share_ratio) :
                config.set('Video Acceleration', "saving_ratio", JSON_data["video_acceleration_ratio"])
    
            if "video" in JSON_data["services"] and is_number(JSON_data["services"]["video"]) and is_in_range (JSON_data["services"]["video"], share_ratio) :
                video_share = config.get('Traffic Gating', "video")
                config.set('Traffic Gating', "video", JSON_data["services"]["video"])
    
            if "file_transfer" in JSON_data["services"] and is_number(JSON_data["services"]["file_transfer"]) and is_in_range (JSON_data["services"]["file_transfer"], share_ratio) :
                file_transfer_share = config.get('Traffic Gating', "file_transfer")
                config.set('Traffic Gating', "file_transfer", JSON_data["services"]["file_transfer"])
    
            if "web_browsing" in JSON_data["services"] and is_number(JSON_data["services"]["web_browsing"]) and is_in_range (JSON_data["services"]["web_browsing"], share_ratio) :
                web_browsing_share = config.get('Traffic Gating', "web_browsing")
                config.set('Traffic Gating', "web_browsing", JSON_data["services"]["web_browsing"])
            
            if (JSON_data["services"]["video"] + JSON_data["services"]["file_transfer"] + JSON_data["services"]["web_browsing"]) != 100 :
                config.set('Traffic Gating', "video", video_share)
                config.set('Traffic Gating', "file_transfer", file_transfer_share)
                config.set('Traffic Gating', "web_browsing", web_browsing_share)
                
            if "vfr" in JSON_data["kpis"] :
                if "wifi"  in JSON_data["kpis"]["vfr"] and is_number(JSON_data["kpis"]["vfr"]["wifi"]) :
                    config.set('Kpi', "wifi_vfr_kpi", JSON_data["kpis"]["vfr"]["wifi"])
                if "to3g"  in JSON_data["kpis"]["vfr"] and is_number(JSON_data["kpis"]["vfr"]["to3g"]) :
                    config.set('Kpi', "to3g_vfr_kpi", JSON_data["kpis"]["vfr"]["to3g"])
    
            if "va" in JSON_data["kpis"] :
                if "wifi"  in JSON_data["kpis"]["va"] and is_number(JSON_data["kpis"]["va"]["wifi"]) :
                    config.set('Kpi', "wifi_va_kpi", JSON_data["kpis"]["va"]["wifi"])
                if "to3g"  in JSON_data["kpis"]["va"] and is_number(JSON_data["kpis"]["va"]["to3g"]) :
                    config.set('Kpi', "to3g_va_kpi", JSON_data["kpis"]["va"]["to3g"])
    
            if "gold" in JSON_data["groups"] and is_number(JSON_data["groups"]["gold"]) :
                config.set('Weights', "gold", JSON_data["groups"]["gold"])
            if "silver_domestic" in JSON_data["groups"] and is_number(JSON_data["groups"]["silver_domestic"]) :
                config.set('Weights', "silver_domestic", JSON_data["groups"]["silver_domestic"])
            if "silver_corporate" in JSON_data["groups"] and is_number(JSON_data["groups"]["silver_corporate"]) :
                config.set('Weights', "silver_corporate", JSON_data["groups"]["silver_corporate"])
            if "bronze_domestic" in JSON_data["groups"] and is_number(JSON_data["groups"]["bronze_domestic"]) :
                config.set('Weights', "bronze_domestic", JSON_data["groups"]["bronze_domestic"])
            if "bronze_corporate" in JSON_data["groups"] and is_number(JSON_data["groups"]["bronze_corporate"]) :
                config.set('Weights', "bronze_corporate", JSON_data["groups"]["bronze_corporate"])
            if "bronze_young" in JSON_data["groups"] and is_number(JSON_data["groups"]["bronze_young"]) :
                config.set('Weights', "bronze_young", JSON_data["groups"]["bronze_young"])
            
            with open(conf_file, 'wb') as cf:
                config.write(cf)
            cf.close();
    
        except (ConfigParser.NoSectionError, ConfigParser.NoOptionError, ConfigParser.Error) :
            logger.error('Configuration file error')
            raise NameError('Configuration error')
    
        #print ('Ending set operation')
        return 
    elif input_type == 'recommendation' :
        return

################################################################
# JSON storage functions
################################################################
def get_preloaded_JSON (logger, simulation_file_name=None) :
    if simulation_file_name == None :
        input_JSON_example = "simulator_input_example3.json"
        input_JSON_example_file = os.path.join(base_file_path, input_JSON_example)
    else :
        input_JSON_example = simulation_file_name + '_input.log'
        input_JSON_example_file = os.path.join(base_file_path, 'log', input_JSON_example)

    try:
        #print 'Processing of input JSON file starts'
        json_data = open(input_JSON_example_file).read()
        input_JSON = json.loads(json_data)
    except Exception as error:
        logger.error(error)
        raise NameError(error)
    else :
        return input_JSON

def store_JSON (id_file, JSON_structure, prefix) :
    if prefix :
        file_name =  id_file + '_' + prefix + '.log'
    else :
        file_name =  id_file + '.log'
    sim_file = os.path.join(base_file_path, 'log', file_name)
    with open(sim_file, 'w') as outfile:
        json.dump(JSON_structure, outfile, indent=4)    

################################################################
# Utility functions
################################################################
def is_number(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

def is_in_range(string, share_type):
    share = float(string)
    if share_type == 'one' :
        if share < 0 and share > 1.0 :
            return False
        else :
            return True
    elif share_type == 'hundred' :
        if share < 0 and share > 100.0 :
            return False
        else :
            return True
