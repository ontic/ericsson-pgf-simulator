# -*- coding: utf-8 -*-

###############################################################################
###                                                                         ### 
### COPYRIGHT (c) Ericsson AB 2015                                          ### 
###                                                                         ### 
### The copyright to the computer Program(s) herein is the                  ### 
### property of Ericsson AB, Sweden. The program(s) may be                  ### 
### used and or copied only with the written permission of                  ### 
### Ericsson AB, or in accordance with the terms and conditions             ### 
### stipulated in the agreement contract under which the                    ### 
### program(s) have been supplied.                                          ### 
###                                                                         ### 
###############################################################################

from django.http import HttpResponse, HttpRequest, JsonResponse, HttpResponseServerError
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

import types

import PGF_simulation_admin

import json
import socket

# some constants
ADMIN_HTML = 'admin.html'
MANAGE_CONFIG_PATH = 'sim_admin/push_config/'

'''# hack to find the machine IP address
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.4.4", 80))
MACHINE_IP = s.getsockname()[0]
s.close()'''

@csrf_exempt
def enable_administration(request):
    # extracts current configuration info and inserts it into the context

    my_result = PGF_simulation_admin.main()
    if my_result["code"] == False :
        return HttpResponseServerError('<b>Server Error</b>:<br/>Not able to access configuration. Contact the tool administrator')
    else :
        context = {'path': MANAGE_CONFIG_PATH, 'json_object': json.dumps(my_result["payload"])}
        return render(request, ADMIN_HTML, context)

@csrf_exempt
def modify_configuration(request) :
    context = {'path': MANAGE_CONFIG_PATH}

    if request.method == 'POST' and request.body :
        my_result = PGF_simulation_admin.main(request.body)
        if my_result["code"] == False :
            return HttpResponseServerError('<b>Server Error</b>:<br/>' + my_result["payload"])
        else :
            return HttpResponse(status=204)
    else :
        return HttpResponse(status=400)