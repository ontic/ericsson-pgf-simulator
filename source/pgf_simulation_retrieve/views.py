# -*- coding: utf-8 -*-

###############################################################################
###                                                                         ### 
### COPYRIGHT (c) Ericsson AB 2015                                          ### 
###                                                                         ### 
### The copyright to the computer Program(s) herein is the                  ### 
### property of Ericsson AB, Sweden. The program(s) may be                  ### 
### used and or copied only with the written permission of                  ### 
### Ericsson AB, or in accordance with the terms and conditions             ### 
### stipulated in the agreement contract under which the                    ### 
### program(s) have been supplied.                                          ### 
###                                                                         ### 
###############################################################################

from django.http import HttpResponseServerError, HttpRequest, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

import pgf_simulation_retrieval

import json

@csrf_exempt
def get_simulation(request, simulation_id) :
    if request.method == 'GET' and len(simulation_id) > 0 :
        print ('Right retrieval request')
        output = pgf_simulation_retrieval.main(simulation_id)
        if output["code"] == True :
            return JsonResponse(output["payload"])
        else :
            return HttpResponseServerError('<b>Server Error</b>:<br/>No session with session_id found')
    else :
        print ('Wrong request. No GET method or not valid session_id')
        return HttpResponseServerError('<b>Server Error</b>:<br/>No GET method or not valid session_id')

@csrf_exempt
def no_simulation(request) :
    return HttpResponseServerError('<b>Server Error</b>:<br/>No or not valid session_id')
