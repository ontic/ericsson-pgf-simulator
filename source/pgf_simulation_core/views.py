# -*- coding: utf-8 -*-

###############################################################################
###                                                                         ### 
### COPYRIGHT (c) Ericsson AB 2015                                          ### 
###                                                                         ### 
### The copyright to the computer Program(s) herein is the                  ### 
### property of Ericsson AB, Sweden. The program(s) may be                  ### 
### used and or copied only with the written permission of                  ### 
### Ericsson AB, or in accordance with the terms and conditions             ### 
### stipulated in the agreement contract under which the                    ### 
### program(s) have been supplied.                                          ### 
###                                                                         ### 
###############################################################################

from django.http import HttpResponseServerError, HttpRequest, JsonResponse
from django.shortcuts import render
from pgf_simulation_core.models import SimulationResult
from django.views.decorators.csrf import csrf_exempt

import PGF_simulation

import json
import socket

# some constants
FE_HTML = 'chart.html'
ADMIN_HTML = 'admin.html'
SIMULATION_PATH = 'sim/simulation/'
DEFAULT_SKIN = 'ericsson'

'''# hack to find the machine IP address
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.4.4", 80))
MACHINE_IP = s.getsockname()[0]
s.close()
'''
@csrf_exempt
def do_simulation(request, simulation_id=None):
    output_text = u''
    payload = None
    file_name = None

    if request.method == 'POST' and request.body :
        print ('Regular request from PGF Mitigation Plan Simulation tool')
        payload = request.body
    elif request.method == 'GET' and simulation_id != None :
        print ('Request to simulate from file from PGF Mitigation Plan Simulation tool')
        file_name = simulation_id
    else :
        print ('Wrong request.')
        return HttpResponseServerError('<b>Server Error</b>:<br/>Wrong request')

    my_result = PGF_simulation.main(payload, file_name)
    if my_result["code"] == True :
        my_json_result = my_result["payload"]
        if 'preloaded' in my_json_result :
            return JsonResponse(json.loads(my_json_result))
        else :
            return JsonResponse(my_json_result, safe=False)
    else :
        return HttpResponseServerError('<b>Server Error</b>:<br/>' + my_result["payload"])

@csrf_exempt
def show_front_end(request):
    # skin extraction
    SKIN = DEFAULT_SKIN
    logger = PGF_simulation.get_PGF_logger()
    conf_data = PGF_simulation.load_conf_data(logger)
    if conf_data["code"] == True :
        SKIN = conf_data["payload"]["general"]["skin"]
    
    context = {'path': SIMULATION_PATH, 'skin': SKIN }

    return render(request, FE_HTML, context)

@csrf_exempt
def no_simulation(request) :
    return HttpResponseServerError('<b>Server Error</b>:<br/>Wrong call to this functionality')
