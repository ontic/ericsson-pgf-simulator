# -*- coding: utf-8 -*-

###############################################################################
###                                                                         ### 
### COPYRIGHT (c) Ericsson AB 2015                                          ### 
###                                                                         ### 
### The copyright to the computer Program(s) herein is the                  ### 
### property of Ericsson AB, Sweden. The program(s) may be                  ### 
### used and or copied only with the written permission of                  ### 
### Ericsson AB, or in accordance with the terms and conditions             ### 
### stipulated in the agreement contract under which the                    ### 
### program(s) have been supplied.                                          ### 
###                                                                         ### 
###############################################################################

import json
import sys
import os.path

main_code_folder = "pgf_simulation_conf"
base_file_path = os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)), main_code_folder)
sys.path.insert(0, base_file_path)
from PGF_simulation_conf import get_PGF_logger, validate_config, validate_JSON_admin_input, validate_JSON_input2, validate_JSON_schema

def main(conf_options=None) :
    logger = get_PGF_logger()
    logger.info('[Simulation tool configuration starts]')
    result_value = {"code": False, "payload": ''}
    #print "PGF_simulation_admin started"

    try :
        conf_data = validate_config (logger)
        logger.info('.Configuration data loaded')
    except NameError as error:
        error_text = 'Configuration validation file error: %s' %(error)
        logger.error(error_text)
        result_value["payload"] = error_text
        return result_value

    if conf_options == None :
        # Configuration options retrieval
        #print 'Get configuration data'
        result_value["code"] = True
        result_value["payload"] = conf_data
        return result_value
    else :
        # Configuration options update
        #print 'Start validation'
        # Validation of JSON schema      
        try :
            validate_JSON_schema (json.loads(conf_options), 'admin', logger)
        except NameError as error:
            error_text = 'New configuration document validation error: %s' %(error)
            logger.error(error_text)
            result_value["payload"] = error_text
            return result_value
        
        # Validation of JSON document semantics
        try :
            #validate_JSON_admin_input (json.loads(conf_options), conf_data, logger)
            validate_JSON_input2 (json.loads(conf_options), conf_data, 'admin', logger)
            result_value["code"] = True
            logger.info('[Simulation administration ends]')
            return result_value
        except NameError as error:
            error_text = 'JSON file validation error: %s' %(error)
            logger.error(error_text)
            logger.error('[Simulation administration aborted]')
            result_value["payload"] = error_text
            return result_value
    
if __name__ == "__main__":
    main()