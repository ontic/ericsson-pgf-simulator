# -*- coding: utf-8 -*-

###############################################################################
###                                                                         ### 
### COPYRIGHT (c) Ericsson AB 2015                                          ### 
###                                                                         ### 
### The copyright to the computer Program(s) herein is the                  ### 
### property of Ericsson AB, Sweden. The program(s) may be                  ### 
### used and or copied only with the written permission of                  ### 
### Ericsson AB, or in accordance with the terms and conditions             ### 
### stipulated in the agreement contract under which the                    ### 
### program(s) have been supplied.                                          ### 
###                                                                         ### 
###############################################################################

import sys
import os.path
import json
import math
from datetime import datetime
from itertools import combinations_with_replacement
from copy import deepcopy
import time
import multiprocessing

conf_code_folder = "pgf_simulation_conf"
main_code_folder = "pgf_simulation_core"
conf_files_dir = os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)), conf_code_folder)
core_files_dir = os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)), main_code_folder)
sys.path.insert(0, conf_files_dir)
sys.path.insert(0, core_files_dir)
from PGF_simulation_conf import control, default_policies, constraint_matrix, get_PGF_logger, validate_JSON_input, validate_JSON_input2, get_PGF_logger
from PGF_simulation import load_conf_data, simulate_plans

def main(payload) :
    message = '{\
            "kpis": [{\
                       "service": "video",\
                       "kpi_names": [{\
                               "name": "video_accessibility"\
                       }, {\
                               "name": "video_freeze_rate"\
                       }]\
            }],\
            "groups": [{\
                       "name": "silver_domestic",\
                       "share": 60\
            }, {\
                       "name": "bronze_domestic",\
                       "share": 0\
            }, {\
                       "name": "bronze_young",\
                       "share": 0\
            }, {\
                       "name": "bronze_corporate",\
                       "share": 0\
            }, {\
                       "name": "silver_corporate",\
                       "share": 0\
            }, {\
                       "name": "gold",\
                       "share": 40\
            }]\
    }'
    message2 = '{\
    "kpis": [\
        {\
            "kpi_names": [\
                {\
                    "name": "video_freeze_rate"\
                },\
                {\
                    "name": "video_accessibility"\
                }\
            ],\
            "service": "video"\
        }\
    ],\
    "policies": [\
        {\
            "policy": "bandwidth_throttling", \
            "parameters": {\
                "limit": 3000\
            }\
        }, \
        {\
            "policy": "bandwidth_throttling", \
            "parameters": {\
                "limit": 1000\
            }\
        }, \
        {\
            "policy": "traffic_gating", \
            "parameters": {\
                "services": [\
                    "file_transfer"\
                ]\
            }\
        }, \
        {\
            "policy": "traffic_gating", \
            "parameters": {\
                "services": [\
                    "web_browsing"\
                ]\
            }\
        }, \
        {\
            "policy": "traffic_gating", \
            "parameters": {\
                "services": [\
                    "video"\
                ]\
            }\
        }, \
        {\
            "policy": "wifi"\
        }, \
        {\
            "policy": "video_acceleration"\
        }, \
        {\
            "policy": "to3g"\
        }, \
        {\
            "policy": "traffic_gating", \
            "parameters": {\
                "services": [\
                    "web_browsing", \
                    "file_transfer"\
                ]\
            }\
        }, \
        {\
            "policy": "traffic_gating", \
            "parameters": {\
                "services": [\
                    "video", \
                    "file_transfer"\
                ]\
            }\
        }, \
        {\
            "policy": "traffic_gating", \
            "parameters": {\
                "services": [\
                    "web_browsing", \
                    "video"\
                ]\
            }\
        }, \
        {\
            "policy": "traffic_gating", \
            "parameters": {\
                "services": [\
                    "video", \
                    "web_browsing", \
                    "file_transfer"\
                ]\
            }\
        }, \
        {\
            "policy": "bandwidth_throttling", \
            "parameters": {\
                "limit": 64\
            }\
        }\
    ], \
    "groups": [\
        {\
            "share": 54, \
            "name": "gold"\
        }, \
        {\
            "share": 8, \
            "name": "silver_domestic"\
        }, \
        {\
            "share": 16, \
            "name": "silver_corporate"\
        }, \
        {\
            "share": 22, \
            "name": "bronze_domestic"\
        }\
    ]\
}'
    '''
    List of default policies:
    [0]{ "policy": "video_acceleration" }, 
    [1]{ "policy": "bandwidth_throttling", "parameters": { "limit": 3000 } }, 
    [2]{ "policy": "bandwidth_throttling", "parameters": { "limit": 1000 } }, 
    [3]{ "policy": "bandwidth_throttling", "parameters": { "limit": 64 } },
    [4]{ "policy": "wifi" }, 
    [5]{ "policy": "to3g" }, 
    [6]{ "policy": "traffic_gating", "parameters": { "services": [ "file_transfer" ] } }, 
    [7]{ "policy": "traffic_gating", "parameters": { "services": [ "web_browsing" ] } }, 
    [8]{ "policy": "traffic_gating", "parameters": { "services": [ "video" ] } }, 
    [9]{ "policy": "traffic_gating", "parameters": { "services": [ "web_browsing", "file_transfer" ] } }, 
    [10]{ "policy": "traffic_gating", "parameters": { "services": [ "video", "file_transfer" ] } }, 
    [11]{ "policy": "traffic_gating", "parameters": { "services": [ "web_browsing", "video" ] } }, 
    [12]{ "policy": "traffic_gating", "parameters": { "services": [ "video", "web_browsing", "file_transfer" ] } }
    [13]{ "policy": "no_policy"}
    '''

    # Logging
    logger = get_PGF_logger('WARNING')
    #simulation_result = {"code": False, "payload": ''}

    # Configuration information validation and load
    conf_data = load_conf_data(logger)
    if conf_data["code"] == True :
        conf_data = conf_data["payload"]
        logger.info('.Configuration data loaded')
    else :
        logger.error(conf_data["payload"])
        return False

    # Recommendation request load
    input_message = json.loads(payload)
    #input_message = json.loads(message)

    # Creation of plans to simulate
    plan_to_simulate = {"groups": '', "kpis": '', "plans": ''}

    # Input groups cleaning
    input_groups = list()
    for group in input_message["groups"] :
        if group["share"] != 0 : input_groups.append(group)
    
    # Policy extraction and combination generation
    available_policies = list()
    available_policies = deepcopy (default_policies)
    available_policies.append({"policy": "no_policy"})

    if (("policies" in input_message) and (len(input_message["policies"]) != 0)) :
        valid_policy_indices = list ()
        for policy in input_message["policies"] :
            if policy["policy"] == 'traffic_gating' :
                policy["parameters"]["services"] = set(policy["parameters"]["services"])
            if policy in default_policies :
                valid_policy_indices.append(default_policies.index(policy))
        valid_policy_indices.append(13)
    
        for group in input_groups :
            for index_truth, value_truth in enumerate (constraint_matrix[group["name"]]) :
                if index_truth not in valid_policy_indices :
                    constraint_matrix[group["name"]][index_truth] = False
            
    combinations = list(combinations_with_replacement(available_policies, len(input_groups)))

    # Time count starts
    start_time = time.time()

    # Good plan parameters
    good_plan_index = -1
    good_plan_enhancement = 0.0
    good_plan_kpis = list()
    plan_counter = 0

    # KPIs to simulate
    kpi_tuples = [a for a in control["kpi"] for kpi in input_message["kpis"] for kpi_item in kpi["kpi_names"] if a[1] == kpi_item["name"]]

    logger.warning ('[Recommendation process starts]')
    logger.warning ('[%d plans available]' % len(combinations))
    
    # limits
    limit_wifi               = float(conf_data["policy_limits"]["wifi"])
    limit_to3g               = float(conf_data["policy_limits"]["to3g"])
    limit_video_acceleration = float(conf_data["policy_limits"]["video_acceleration"])

    for item_index, item in enumerate(combinations) :
        no_valid_plan = False
        # each roundtrip aims to simulate a plan
        # First, we determine whether the plan is valid (according to the limits for 
        #Wifi Offload, Switch to 3G or Video Acceleration)
        total_wifi               = 0.0
        total_to3g               = 0.0
        total_video_acceleration = 0.0
        for index, policy in enumerate(item):
            group_name = input_groups[index]["name"]
            group_truth_array = constraint_matrix[group_name]
            policy_index = available_policies.index(policy)
            if group_truth_array[policy_index] == False :
                no_valid_plan = True
                break
            if policy["policy"] == 'wifi' :
                total_wifi = total_wifi + float(input_groups[index]["share"])
                if total_wifi > limit_wifi : break
            elif policy["policy"] == 'to3g' :
                total_to3g = total_to3g + float(input_groups[index]["share"])
                if total_to3g > limit_to3g : break
            elif policy["policy"] == 'video_acceleration' :
                total_video_acceleration = total_video_acceleration + float(input_groups[index]["share"])
                if total_video_acceleration > limit_video_acceleration : break
        if no_valid_plan == True : continue
        if total_wifi > limit_wifi or total_to3g > limit_to3g or total_video_acceleration > limit_video_acceleration : continue
        else : plan_counter = plan_counter + 1

        # Next, we assemble the plan to simulate
        plan_to_simulate["groups"] = deepcopy(input_groups)
        plan_to_simulate["kpis"] = deepcopy(input_message["kpis"])
        plan_to_simulate["plans"] = list()
        plan_to_simulate["plans"].append({"plan_ref": str(item_index), "policies": list()})

        for index, policy in enumerate(item) :
            target_policy = dict()
            target_policy = deepcopy(policy)
            group_name = deepcopy(input_groups[index]["name"])
            target_policy["group"] = group_name
            plan_to_simulate["plans"][0]["policies"].append(target_policy)
                
        # Validation of JSON document semantics and plan to simulate content finishing
        try :
            input_data = validate_JSON_input2 (plan_to_simulate, conf_data, 'simulation', logger)
        except NameError as error:
            #print error
            return False

        # Actual simulation
        simulation_result = simulate_plans (input_data, conf_data, False, logger)

        # Best plan search
        weighted_kpi_enhancement = 0.0
        plan_index = int(simulation_result["results"][0]["plan"])
        print 'Plan ' + simulation_result["results"][0]["plan"] + ' simulated'
        
        # Best plan computation by averaging the weighted KPI enhancement for every KPI
        kpi_enhancement_container = list()
        for kpi_tuple in kpi_tuples :
            sim_kpi_result = [kpi_result for kpi_result in simulation_result["results"][0]["kpis"] if kpi_result["kpi_name"] == kpi_tuple[0]]
            sim_kpi_tuple = deepcopy(sim_kpi_result[0])
            sim_kpi_tuple.pop("kpi_groups", None)
            kpi_enhancement_container.append(sim_kpi_tuple)
            weighted_kpi_enhancement = weighted_kpi_enhancement + sim_kpi_result[0]["weighted_delta_value"]

        print 'KPI enhancement is %.2f' %(weighted_kpi_enhancement)
        if (weighted_kpi_enhancement/len(kpi_tuples)) > good_plan_enhancement :
            good_plan_enhancement = weighted_kpi_enhancement/len(kpi_tuples)
            good_plan_index = plan_index
            good_plan_kpis = deepcopy(kpi_enhancement_container)
            
    print('Good plan is %s: %f %%' %(good_plan_index, good_plan_enhancement))
    logger.warning ('[%d plans tested]' % (plan_counter))
    logger.warning ('[Good plan is %s: %f %%]' %(good_plan_index, good_plan_enhancement))
    #good_plan = combinations[good_plan_index]
    #print("--- %s seconds ---" % (time.time() - start_time))
    logger.warning ("[Time taken: --- %s seconds ---]" % (time.time() - start_time))
    logger.warning ('[Recommendation process ends]')
    
    good_plan = list()
    recommendation_result = dict()
    for policy_index, policy in enumerate(combinations[good_plan_index]) :
        item = dict()
        item = deepcopy(policy)
        if "parameters" in item and "services" in item["parameters"] :
            item["parameters"]["services"] = list(item["parameters"]["services"])
        item["group"] = input_groups[policy_index]["name"]
        good_plan.append(item)
    recommendation_result["elapsed_time"] = math.ceil((time.time() - start_time)*1000)/1000
    recommendation_result["tested_plans"] = plan_counter
    recommendation_result["available_plans"] = len(combinations)
    recommendation_result["plan"] = good_plan
    recommendation_result["kpis"] = good_plan_kpis
    recommendation_result["date"] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    #print json.dumps(recommendation_result)
    return json.dumps(recommendation_result)

if __name__ == "__main__":
    main()