# -*- coding: utf-8 -*-

import sys
import os.path

main_code_folder = "pgf_simulation_conf"
base_file_path = os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)), main_code_folder)
sys.path.insert(0, base_file_path)
from PGF_simulation_tools import normalized_kpi_gain, kpi_no_policies
from PGF_simulation_conf import control, get_preloaded_JSON, validate_config, validate_JSON_input, validate_JSON_schema, validate_JSON_input2, get_PGF_logger, store_JSON

import json
import uuid
from datetime import datetime
from time import gmtime, strftime
from copy import deepcopy
from multiprocessing import Pool

############################################################################################################################
# main function receives the following arguments:
# - request_payload: JSON text passed on as HTTP body from the simulation front-end
# - simulation_file_name: simulation id so that the simulation is run from the input of an already executed simulation
# - output: flag stating whether verbose logging and simulation input and output simulation must be stored
############################################################################################################################
def main(request_payload=None, simulation_file_name=None, output=True) :
    # Definition of the level of logging. Output is True in regular regulation requests and False in recommendations
    if output == True :
        logger = get_PGF_logger()
    else :
        logger = get_PGF_logger('ERROR')

    # Multiprocessing
    pool = Pool()

    # Variable for passing on results (consider using a class when refactoring; first item is a status, boolean, code; 
    #second one is the actual per plan result structure, if code is True)
    simulation_result = {"code": False, "payload": ''}

    # Configuration information validation and load
    conf_data = load_conf_data(logger)
    if conf_data["code"] == True :
        conf_data = conf_data["payload"]
        logger.info('.Configuration data loaded')
    else :
        simulation_result["payload"] = conf_data["payload"]
        logger.error(conf_data["payload"])
        return simulation_result
    
    # Simulation request information validation and load
    ##################################    
    return_value = {"code": False, "payload": ''}
    JSON_request = ''
    
    # First we retrieve the plan simulation request. It can come from the front-end or
    #be preloaded.
    retrieved_value = retrieve_input(request_payload, simulation_file_name, logger)
    if retrieved_value["code"] == False :
        simulation_result["payload"] = retrieved_value["payload"]
        logger.error(simulation_result["payload"])
        return simulation_result
    else :
        JSON_request = retrieved_value["payload"]

    # Next, we validate the JSON schema
    try :
        validate_JSON_schema (JSON_request, 'simulation', logger)
    except NameError as error:
        error_text = 'JSON schema validation error: %s' %(error)
        simulation_result["payload"] = error_text
        logger.error(simulation_result["payload"])
        return simulation_result
        
    # Validation of JSON document semantics and content finishing
    try :
        #validate_JSON_admin_input (JSON_request, conf_data, logger)
        input_data = validate_JSON_input2 (JSON_request, conf_data, 'simulation', logger)
        #input_data = input_data["payload"]
    except NameError as error:
        error_text = 'JSON file validation error: %s' %(error)
        logger.error(error_text)
        logger.error('[Simulation aborted]')
        simulation_result["payload"] = error_text
        #logger.error(input_data["payload"])
        return simulation_result
    ##################################
    '''
    input_data = generate_input (request_payload, conf_data, simulation_file_name, logger)
    if input_data["code"] == True :
        input_data = input_data["payload"]
    else :
        simulation_result["payload"] = input_data["payload"]
        logger.error(input_data["payload"])
        return simulation_result
    '''
    ##################################

    # request logging
    if output == True :
        try :
            pool.apply_async(store_JSON, args=(simulation_results["id"], JSON_input_data, 'input',))
            #store_JSON (simulation_results["id"], JSON_input_data, 'input')
            logger.info('.Simulation request stored in %s_input.log' % (simulation_results["id"]))
        except :
            logger.warning('Simulation request cannot be stored')
            pass

    # actual simulation
    if request_payload == None : simulated=True
    else: simulated=False
    simulation_results = simulate_plans (input_data, conf_data, simulated, logger)

    # request_id extraction
    if request_payload != None :
        JSON_request = json.loads(request_payload)
        if 'source' in JSON_request and JSON_request["source"] == u'main' and 'request_id' in JSON_request :
            simulation_results["request_id"] = JSON_request["request_id"]

    if output == True :
        try :
            #print json.dumps(simulation_results,indent=4)
            pool.apply_async(store_JSON, args=(simulation_results["id"], simulation_results, 'output',))
            #store_JSON (simulation_results["id"], simulation_results, 'output')
            logger.info('.Simulation results stored in %s_output.log' % (simulation_results["id"]))
        except :
            logger.warning('Simulation results cannot be saved')
            pass

    simulation_result["code"] = True
    simulation_result["payload"] = json.dumps(simulation_results,indent=4)
    return simulation_result

def load_conf_data (logger) :
    return_value = {"code": False, "payload": ''}

    try :
        configuration_data = validate_config (logger)
        return_value["code"] = True
        return_value["payload"] = configuration_data
        return return_value
    except NameError as error:
        return_value["payload"] = ('Configuration validation file error: %s' %(error))
        return return_value

def retrieve_input (request, sim_file_name, logger) :
    return_value = {"code": False, "payload": ''}
    JSON_request = ''

    # We retrieve the plan simulation request. It can come from the front-end or be preloaded.
    if request == None :
        # the simulator uses a preloaded JSON file (through get_example_JSON)
        try :
            JSON_request = get_preloaded_JSON (logger, sim_file_name)
            return_value["code"] = True
            return_value["payload"] = JSON_request
            logger.info('.Input data loaded (from preloaded file)')
            return return_value
        except NameError as error:
            return_value["payload"] = 'JSON file read operation error: %s' %(error)
            return return_value
    else :
        # the simulator uses the input sent from the simulator application
        try :
            JSON_request = json.loads(request)
            return_value["code"] = True
            return_value["payload"] = JSON_request
            logger.info('.Input data loaded')
            return return_value
        except Exception as error:
            return_value["payload"] = 'JSON input not valid %s' %(error)
            return return_value
    
def generate_input (request, config_data, sim_file_name, logger) :
    return_value = {"code": False, "payload": ''}
    JSON_request = ''
    
    # First we retrieve the plan simulation request. It can come from the front-end or
    #be preloaded.
    retrieved_value = retrieve_input(request, sim_file_name, logger)
    if retrieved_value["code"] == False :
        return_value["payload"] = retrieved_value["payload"]
        return return_value
    else :
        JSON_request = retrieved_value["payload"]

    # Next, we validate the JSON schema
    try :
        validate_JSON_schema (JSON_request, 'simulation', logger)
    except NameError as error:
        error_text = 'JSON schema validation error: %s' %(error)
        logger.error(error_text)
        return_value["payload"] = error_text
        return return_value
        
    # Validation of JSON document semantics
    try :
        #validate_JSON_admin_input (JSON_request, conf_data, logger)
        return_data = validate_JSON_input2 (JSON_request, config_data, 'simulation', logger)
        return_value["code"] = True
        return_value["payload"] = return_data
        return return_value
    except NameError as error:
        error_text = 'JSON file validation error: %s' %(error)
        logger.error(error_text)
        logger.error('[Simulation aborted]')
        return_value["payload"] = error_text
        return return_value    
    '''
    try :
        return_data = validate_JSON_input (JSON_request, config_data, logger)
        logger.info('.Input data validated')
        return_value["code"] = True
        return_value["payload"] = return_data
        return return_value
    except NameError as error:
        return_value["payload"] = 'JSON file validation error: %s' %(error)
        return return_value
    '''

def simulate_plans (input_data, conf_data, simulated, logger) :
    # First, we define the dictionary that will store the simulation results
    simulation_results = dict()                                                 # see simulation_output.json for further details
    simulation_results["id"] = str(uuid.uuid4())                                # a unique simulation identifier
    simulation_results["date"] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')   # simulation date
    simulation_results["results"] = list()                                      # per plan simulation results

    # The following information is needed for starting a simulation:
    # - Subscriber group information (name, share) -> input_data["shares"]
    # - KPI to simulate -> input_data["services"]
    # - Policies to be applied to each subscriber groups (type, parameters, 
    # subscriber groups the policy applies to) -> input_data["plans"]
    # - Functions and parameters for computing KPI gain -> conf_data["general"]

    # Simulation of each plan (plan_name extracted from input_data["plans"])
    for plan_name in input_data["plans"] :
        # For each plan, we fill per_plan_result. At the end, we append it to 
        #simulation_results["results"]
        per_plan_result = dict()
        per_plan_result["plan"] = plan_name
        per_plan_result["groups"] = list() 
        per_plan_result["kpis"] = list()

        if logger != None : logger.info ('.Simulation of plan %s starts' %(plan_name))

        # In order to fill per_plan_result["groups"], we extract the 'actions' in each plan. 
        #An action defines which mitigation actions ('policy') undergoes each group ('group'). 
        #Actions can be applied according to different parameters ('parameters')
        for action in input_data["plans"][plan_name] :
            # Creation of temporary dictionaries to be stored later in per_plan_result["groups"] 
            #(it is a list of dictionaries). Each temporary dictionary stores results not 
            #tied to KPI gain computation, such as 'group', 'policy', 'share', 'susbscribers' 
            #and weights. Once computed, it is appended to per_plan_result["groups"]:
            # - intermediate_group_results: usual dictionary
            # - intermediate_wifigroup_results stores results for a fictitious group 
            #   (policy: 'no_wiki') for those users deemed to be switched to wifi but 
            #   without actual ANDSF support
            intermediate_group_results = dict()
            intermediate_wifigroup_results = dict()

            intermediate_group_results["group"] = deepcopy(action["group"])
            intermediate_group_results["policy"] = deepcopy(action["policy"])
            if ((action["policy"] == 'traffic_gating' or action["policy"] == 'bandwidth_throttling') and "parameters" in action) :
                intermediate_group_results["policy_parameters"] = deepcopy(action["parameters"])
            if action["policy"] == 'wifi' :
                intermediate_group_results["share"] = input_data["shares"][action["group"]] * float(conf_data["general"]["andsf_ratio"])/conf_data["general"]["ratio_factor"]
            else :
                intermediate_group_results["share"] = input_data["shares"][action["group"]]
            #### Comment: should the following statement follow the previous if?
            intermediate_group_results["subscribers"] = float(conf_data["general"]["max_users"]) * (intermediate_group_results["share"] / conf_data["general"]["ratio_factor"])
            intermediate_group_results["weights"] = conf_data["weights"][action["group"]]
            per_plan_result["groups"].append(intermediate_group_results)

            if action["policy"] == 'wifi' :
                intermediate_wifigroup_results["group"] = deepcopy(action["group"])
                intermediate_wifigroup_results["policy"] = 'no_wifi'
                intermediate_wifigroup_results["share"] = input_data["shares"][action["group"]] - intermediate_group_results["share"]
                intermediate_wifigroup_results["subscribers"] = float(conf_data["general"]["max_users"]) * (intermediate_wifigroup_results["share"] / conf_data["general"]["ratio_factor"])
                intermediate_wifigroup_results["weights"] = deepcopy(intermediate_group_results["weights"])
                #print extra_temp_results
                per_plan_result["groups"].append(intermediate_wifigroup_results)     #new

        # In order to fill per_plan_result["kpis"], we determine the services and KPIs that
        #must be simulated in each plan and simulate each of them
        kpis = [(service, kpi) for service in input_data["services"] for kpi in input_data["services"][service]]
        for service, kpi in kpis :
            if logger != None : logger.info('...Simulation of %s (%s) starts' % (kpi, service))
            # Creation of temporary dictionaries for the results associated to each KPI.
            #intermediate_kpi_results stores results tied to KPI gain computation. Once computed, 
            #it is appended to per_plan_result["kpis"]
            intermediate_kpi_results = dict()
            short_kpi_name = dict(control["kpi"]).keys()[dict(control["kpi"]).values().index(kpi)] # translation from long to short name
            intermediate_kpi_results["kpi_name"] = short_kpi_name
            intermediate_kpi_results["value_type"] = conf_data["general"]["global_kpi_ratio"]
            intermediate_kpi_results["kpi_groups"] = list()

            # Variables used to compute total KPI and weighted KPI
            total_kpi = 0.0
            total_weighted_kpi = 0.0

            if short_kpi_name in conf_data["supported_kpis"] :
                # ENHANCEMENT: verify what to do if this verification is negative
                subscribers_with_no_a = float(conf_data["general"]["max_users"])
                # 1. Determination of type 'a' policies (steps 1, 2 and 3)
                #---actions = [i for i in simulation_results[plan_name] if (i["policy"] == "wifi" or i["policy"] == "to3g")]
                actions = [i for i in per_plan_result["groups"] if (i["policy"] == "wifi" or i["policy"] == "to3g")]
                # 2. Computation of definite KPI value and KPI gain for users undergoing type 'a' policies
                for action in actions :
                    #print '+', result["policy"]
                    if action["policy"] == "wifi" : 
                        if logger != None : logger.info('.....Wifi policy found for group %s. Simulating' %(action["group"]))
                        new_kpi_value = float (conf_data["supported_kpis"][short_kpi_name]["wifi"])
                    elif action["policy"] == "to3g" : 
                        if logger != None : logger.info('.....Switch to 3G policy found for group %s. Simulating' %(action["group"]))
                        new_kpi_value = float (conf_data["supported_kpis"][short_kpi_name]["to3g"])
                    kpi_gain = normalized_kpi_gain (float (conf_data["supported_kpis"][short_kpi_name]["threshold"]), 
                                                    new_kpi_value, 
                                                    float (conf_data["supported_kpis"][short_kpi_name]["worst"]), 
                                                    float (conf_data["supported_kpis"][short_kpi_name]["best"]), 
                                                    float (conf_data["supported_kpis"][short_kpi_name]["threshold"]))
                    intermediate_kpi_results["kpi_groups"].append({"name":action["group"], "kpi_gain":kpi_gain, "value_type": conf_data["general"]["individual_kpi_ratio"]})
                    total_kpi = total_kpi + action["share"] * kpi_gain
                    total_weighted_kpi = total_weighted_kpi + (action["share"] * kpi_gain * int(action["weights"]) / max(int(i) for i in conf_data["weights"].values()))

                    # 3. Determination of the amount of users not undergoing type 'a' policies
                    subscribers_with_no_a = subscribers_with_no_a - float(action["subscribers"])

                # 4. Computation of theoretical KPI value for users not undergoing type 'a' policies
                ##### COMMENT: If no type a users, a KPI value is received. Why? It's because the function does not exactly crosses the axis in the right point
                #intermediate_kpi_for_no_a = kpi_no_policies (conf_data["supported_kpis"][short_kpi_name], conf_data["general"]["kpi_gain_function"], conf_data["general"]["kpi_gain_parameter"], subscribers_with_no_a)
                intermediate_kpi_for_no_a = kpi_no_policies (conf_data, short_kpi_name, subscribers_with_no_a)

                # 5. Determination of groups undergoing type 'b' policies
                actions = [i for i in per_plan_result["groups"] if (i["policy"] == "bandwidth_throttling" or i["policy"] == "traffic_gating")]
                equivalent_removed_users = 0.0
                for action in actions :
                    #print "plan", plan, "policy", result["policy"], "group", result["group"]
                    if action["policy"] == "bandwidth_throttling" : 
                        if logger != None : logger.info('.....Bandwidth throttling policy found for group %s. Simulating' %(action["group"]))
                        action_definition = next((item for item in input_data["plans"][plan_name] if item["group"] == action["group"] and item["policy"] == action["policy"]), None)
                        bandwidth_limit = action_definition["parameters"]["limit"]
                        #bandwidth_limit = get_item("group", action["group"], "policy", action["policy"], 
                        #              input_data["plans"][plan_name])["parameters"]["limit"]
                        if bandwidth_limit == 3000 :
                            action["policy"] = "no_bandwidth_throttling" # in fact, this policy is considered a 'no policy'
                        elif bandwidth_limit == 1000 :
                            equivalent_removed_users = equivalent_removed_users + 2*action["subscribers"]/3     # as if 2/3 of users where removed
                            #print 'equivalent users', equivalent_removed_users
                            new_kpi_value = intermediate_kpi_for_no_a + (2*abs(float(conf_data["supported_kpis"][short_kpi_name]["best"])-intermediate_kpi_for_no_a)/3)         # new theoretical KPI value
                            #print 'new KPI value', new_kpi_value
                            kpi_gain = normalized_kpi_gain (float (conf_data["supported_kpis"][short_kpi_name]["threshold"]), 
                                                            new_kpi_value, 
                                                            float (conf_data["supported_kpis"][short_kpi_name]["worst"]), 
                                                            float (conf_data["supported_kpis"][short_kpi_name]["best"]), 
                                                            float (conf_data["supported_kpis"][short_kpi_name]["threshold"]))
                            intermediate_kpi_results["kpi_groups"].append({"name":action["group"], "kpi_gain":kpi_gain, "value_type": conf_data["general"]["individual_kpi_ratio"]})
                            total_kpi = total_kpi + action["share"] * kpi_gain
                            total_weighted_kpi = total_weighted_kpi + (action["share"] * kpi_gain * int(action["weights"]) / max(int(i) for i in conf_data["weights"].values()))
                            #print '       KPI value for bandwidth throttling policy for group '+result["group"]+' is: '+str(new_kpi_value)+' ('+str(result[short_kpi_name+"_gain"]*conf_general_dict ['ratio_factor'])+'%)'
                        elif bandwidth_limit == 64 :
                            equivalent_removed_users = equivalent_removed_users + action["subscribers"]         # as if all the users where removed
                            #print 'equivalent users', equivalent_removed_users
                            new_kpi_value = float (conf_data["supported_kpis"][short_kpi_name]["worst"])        # worst KPI value
                            #print 'new KPI value', new_kpi_value
                            kpi_gain = normalized_kpi_gain (float (conf_data["supported_kpis"][short_kpi_name]["threshold"]), 
                                                            new_kpi_value, 
                                                            float (conf_data["supported_kpis"][short_kpi_name]["worst"]), 
                                                            float (conf_data["supported_kpis"][short_kpi_name]["best"]), 
                                                            float (conf_data["supported_kpis"][short_kpi_name]["threshold"]))
                            intermediate_kpi_results["kpi_groups"].append({"name":action["group"], "kpi_gain":kpi_gain, "value_type": conf_data["general"]["individual_kpi_ratio"]})
                            total_kpi = total_kpi + action["share"] * kpi_gain
                            total_weighted_kpi = total_weighted_kpi + (action["share"] * kpi_gain * int(action["weights"]) / max(int(i) for i in conf_data["weights"].values()))
                            #print '       KPI value for bandwidth throttling policy for group '+result["group"]+' is: '+str(new_kpi_value)+' ('+str(result[short_kpi_name+"_gain"]*conf_general_dict ['ratio_factor'])+'%)'
                    elif action["policy"] == "traffic_gating" :
                        action_definition = next((item for item in input_data["plans"][plan_name] if item["group"] == action["group"] and item["policy"] == action["policy"]), None)
                        gated_services = action_definition["parameters"]["services"]
                        #gated_services = get_item("group", action["group"], "policy", action["policy"], 
                        #               input_data["plans"][plan_name])["parameters"]["services"]
                        if logger != None : logger.info('.....Traffic gating policy found for group %s (KPI simulation for service %s). Simulating' %(action["group"], service))

                        for gated_service in gated_services :
                            if service == gated_service : equivalent_removed_users = equivalent_removed_users + (action["subscribers"]*float(conf_data["services"][service])/100)

                        if service in gated_services :
                            if logger != None : logger.info('.....Traffic gating applies to services %s: match' %(', '.join(gated_services)))
                            new_kpi_value = float (conf_data["supported_kpis"][short_kpi_name]["worst"])        # worst KPI value
                            #print 'new KPI value', new_kpi_value
                            kpi_gain = normalized_kpi_gain (float (conf_data["supported_kpis"][short_kpi_name]["threshold"]), 
                                                            new_kpi_value, 
                                                            float (conf_data["supported_kpis"][short_kpi_name]["worst"]), 
                                                            float (conf_data["supported_kpis"][short_kpi_name]["best"]), 
                                                            float (conf_data["supported_kpis"][short_kpi_name]["threshold"]))
                            intermediate_kpi_results["kpi_groups"].append({"name":action["group"], "kpi_gain":kpi_gain, "value_type": conf_data["general"]["individual_kpi_ratio"]})
                            total_kpi = total_kpi + action["share"] * kpi_gain
                            total_weighted_kpi = total_weighted_kpi + (action["share"] * kpi_gain * int(action["weights"]) / max(int(i) for i in conf_data["weights"].values()))
                            #print '       KPI value for traffic gating policy ('+service+') for group '+result["group"]+' is: '+str(new_kpi_value)+' ('+str(result[short_kpi_name+"_gain"]*conf_general_dict ['ratio_factor'])+'%)'
                        else :
                            if logger != None : logger.info('.....Traffic gating applies to services %s: no match' %(', '.join(gated_services)))
                            action["policy"] = "no_traffic_gating"
                            #print '       KPI value for traffic gating policy for group '+result["group"]+' cannot be computed yet'
                # Intermediate KPI value for uses that do not undergo type 'a' or 'b' policies
                subscribers_with_no_b = subscribers_with_no_a - equivalent_removed_users
                ##### COMMENT: shouldn't this function consider the starting point of the KPI enhancement?
                #intermediate_kpi_for_no_b = kpi_no_policies (conf_data["supported_kpis"][short_kpi_name], conf_data["general"]["kpi_gain_function"], conf_data["general"]["kpi_gain_parameter"], subscribers_with_no_b)
                intermediate_kpi_for_no_b = kpi_no_policies (conf_data, short_kpi_name, subscribers_with_no_b)
                #print 'intermediate kpi', intermediate_kpi_for_no_b

                actions = [i for i in per_plan_result["groups"] if (i["policy"] == "video_acceleration")]
                for action in actions :
                    if logger != None : logger.info('.....Video acceleration gating policy found for group %s. Simulating' %(action["group"]))
                    ##### COMMENT: shouldn't have equivalent_removed_users been resetted?
                    ##### COMMENT: verify whether the multiplication below is right!!!
                    equivalent_removed_users = equivalent_removed_users + (action["subscribers"]*float(conf_data["general"]['video_acc_saving_ratio'])/100)
                    kpi_gain = normalized_kpi_gain (float (conf_data["supported_kpis"][short_kpi_name]["threshold"]), 
                                                    intermediate_kpi_for_no_b, 
                                                    float (conf_data["supported_kpis"][short_kpi_name]["worst"]), 
                                                    float (conf_data["supported_kpis"][short_kpi_name]["best"]), 
                                                    float (conf_data["supported_kpis"][short_kpi_name]["threshold"]))
                    intermediate_kpi_results["kpi_groups"].append({"name":action["group"], "kpi_gain":kpi_gain, "value_type": conf_data["general"]["individual_kpi_ratio"]})
                    total_kpi = total_kpi + action["share"] * kpi_gain
                    total_weighted_kpi = total_weighted_kpi + (action["share"] * kpi_gain * int(action["weights"]) / max(int(i) for i in conf_data["weights"].values()))
                    #print '       KPI value for policy '+result["policy"]+' for group '+result["group"]+' is '+str(intermediate_kpi_for_no_b)+' ('+str(result[short_kpi_name+"_gain"]*conf_general_dict ['ratio_factor'])+'%)'

                #intermediate_kpi_for_no_c = kpi_no_policies (conf_data["supported_kpis"][short_kpi_name], conf_data["general"]["kpi_gain_function"], conf_data["general"]["kpi_gain_parameter"], subscribers_with_no_a - equivalent_removed_users)
                intermediate_kpi_for_no_c = kpi_no_policies (conf_data, short_kpi_name, subscribers_with_no_a - equivalent_removed_users)
                for action in per_plan_result["groups"] :
                    if action["policy"] == 'no_wifi' or action["policy"] == 'no_traffic_gating' or action["policy"] == 'no_bandwidth_throttling' or action["policy"] =="no_policy":
                        kpi_gain = normalized_kpi_gain (float (conf_data["supported_kpis"][short_kpi_name]["threshold"]), 
                                                        intermediate_kpi_for_no_c, 
                                                        float (conf_data["supported_kpis"][short_kpi_name]["worst"]), 
                                                        float (conf_data["supported_kpis"][short_kpi_name]["best"]), 
                                                        float (conf_data["supported_kpis"][short_kpi_name]["threshold"]))
                        intermediate_kpi_results["kpi_groups"].append({"name":action["group"], "policy":'No', "kpi_gain":kpi_gain, "value_type": conf_data["general"]["individual_kpi_ratio"]})
                        total_kpi = total_kpi + action["share"] * kpi_gain
                        total_weighted_kpi = total_weighted_kpi + (action["share"] * kpi_gain * int(action["weights"]) / max(int(i) for i in conf_data["weights"].values()))

                #intermediate_kpi_results["delta_value"] = '%s' % (total_kpi)
                #intermediate_kpi_results["weighted_delta_value"] = '%s' % (total_weighted_kpi)
                intermediate_kpi_results["delta_value"] = total_kpi
                intermediate_kpi_results["weighted_delta_value"] = total_weighted_kpi
                per_plan_result["kpis"].append(intermediate_kpi_results)
            else  :
                if logger != None : logger.warning('KPI % not supported. Not computed' % (kpi))
                #logging.warning('KPI % not supported. Not computed' % (kpi))

            if logger != None : logger.info('...Simulation of %s (%s) ends' % (kpi, service))

        # the processing for this plan is over. Let's append the per plan results to simulation_results["results"]
        simulation_results["results"].append(per_plan_result)
        #print '       KPI value for policy '+result["policy"]+' for group '+result["group"]+' is '+str(intermediate_kpi_for_no_c)+' ('+str(result[short_kpi_name+"_gain"]*conf_general_dict ['ratio_factor'])+'%)'
        if logger != None : logger.info ('.Simulation of plan %s ends' %(plan_name))

    # simulation results are sent back
    if simulated == True :
        # if simulation is carried out on a preloaded file, say it!!
        simulation_results["remarks"] = u'The results passed on by this JSON file have been generated by a preloaded plan description and do not come from any mitigation plan simulation request'

    if logger != None : logger.info('[Simulation %s (%s)] ends\n\n' % (simulation_results["id"], simulation_results["date"]))
    return simulation_results

if __name__ == "__main__":
    main()