# -*- coding: utf-8 -*-

###############################################################################
###                                                                         ### 
### COPYRIGHT (c) Ericsson AB 2015                                          ### 
###                                                                         ### 
### The copyright to the computer Program(s) herein is the                  ### 
### property of Ericsson AB, Sweden. The program(s) may be                  ### 
### used and or copied only with the written permission of                  ### 
### Ericsson AB, or in accordance with the terms and conditions             ### 
### stipulated in the agreement contract under which the                    ### 
### program(s) have been supplied.                                          ### 
###                                                                         ### 
###############################################################################

import sys
import json
import os.path
from datetime import datetime

main_code_folder = "pgf_simulation_conf"
base_file_path = os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)), main_code_folder)
sys.path.insert(0, base_file_path)
from PGF_simulation_conf import get_PGF_logger

main_log_folder = os.path.join(base_file_path, 'log')

def main(session_id=None) :
    logger = get_PGF_logger()
    logger.info('[Retrieval of simulation with id %s (%s)] starts' % (session_id, datetime.now().strftime('%Y-%m-%d %H:%M:%S')))

    result = {"code": False, "payload": ''}
    try :
        with open(os.path.join (main_log_folder, session_id+'_output.log'), 'r') as content_file:
            content = content_file.read()
        result["code"] = True
        result["payload"] = json.loads(content)
        logger.info('.Successfull retrieval of simulation')
    except :
        pass
    logger.info('[Retrieval of simulation with id %s] ends\n\n' % (session_id))
    return result

if __name__ == "__main__":
    main()