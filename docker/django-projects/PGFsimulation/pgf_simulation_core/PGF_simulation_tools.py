# -*- coding: utf-8 -*-

###############################################################################
###                                                                         ### 
### COPYRIGHT (c) Ericsson AB 2015                                          ### 
###                                                                         ### 
### The copyright to the computer Program(s) herein is the                  ### 
### property of Ericsson AB, Sweden. The program(s) may be                  ### 
### used and or copied only with the written permission of                  ### 
### Ericsson AB, or in accordance with the terms and conditions             ### 
### stipulated in the agreement contract under which the                    ### 
### program(s) have been supplied.                                          ### 
###                                                                         ### 
###############################################################################

"""
PGF simulation tools

This file provides some supporting functions for the policy enforcement
simulation functionality.

The following functions are provided:
- linear: a mathematical function implementing y = n/u
- normalized_exponentiation: a mathematical function implementing 1 - n**parameter
- normalized_sigmoid: a mathematical function implementing a normalized sigmoid

"""

from math import exp, pow

# Mathematical functions
def lineal(n,u):
    return n/float(u)

def normalized_exponentiation(n, parameter):
    return 1 - pow(parameter,n)
  
def normalized_sigmoid(n, parameter):
    n = n*parameter
    return (2 / (1 + exp(-n)))-1

# KPI gain computation functions
def normalized_kpi_gain(start_kpi, end_kpi, worst_kpi, best_kpi, threshold_pki):
    # kpi gain is a ratio (per one)
    upstream = True
    if (best_kpi-worst_kpi) < 0 : upstream = False
    
    best_to_threshold = abs(best_kpi-threshold_pki)
    worst_to_threshold = abs(worst_kpi-threshold_pki)
    
    if upstream : gain = end_kpi-start_kpi
    elif not upstream : gain = start_kpi-end_kpi

    if gain > 0 :
        return gain / abs(best_kpi-threshold_pki)
    else :
        return gain / abs(worst_kpi-threshold_pki)

def kpi_gain(start_kpi, end_kpi):
    # kpi gain is a ratio (per one)
    return (start_kpi-end_kpi)/start_kpi

#def kpi_no_policies(kpi_values, function_type, function_parameter, n) :
def kpi_no_policies(config_data, short_kpi_name, n) :
    # it describes the enhancement of the KPI value

    # parameters extraction
    kpi_values = config_data["supported_kpis"][short_kpi_name]
    function_type = config_data["general"]["kpi_gain_function"]
    if function_type == 'logistic' or function_type == 'exponentiation' :
        function_parameter = config_data["general"]["kpi_gain_parameter"]
    
    upstream = True
    if float (kpi_values['worst']) - float (kpi_values['best']) > 0: upstream = False

    function_result=0.0
    if function_type == 'logistic' :
        function_result = normalized_sigmoid(n, float(function_parameter))
    elif function_type == 'exponentiation' :
        function_result = normalized_exponentiation(n, float(function_parameter))
    elif function_type == 'linear' :
        function_result = lineal(n, config_data["general"]["max_users"])

    if upstream :
        return float(kpi_values['threshold']) + (1-function_result)*abs(
               float(kpi_values['best']) - 
               float(kpi_values['threshold']))
    else :
        return float(kpi_values['best']) + function_result*abs(
            float(kpi_values['best']) - 
            float(kpi_values['threshold']))
