# -*- coding: utf-8 -*-

###############################################################################
###                                                                         ### 
### COPYRIGHT (c) Ericsson AB 2015                                          ### 
###                                                                         ### 
### The copyright to the computer Program(s) herein is the                  ### 
### property of Ericsson AB, Sweden. The program(s) may be                  ### 
### used and or copied only with the written permission of                  ### 
### Ericsson AB, or in accordance with the terms and conditions             ### 
### stipulated in the agreement contract under which the                    ### 
### program(s) have been supplied.                                          ### 
###                                                                         ### 
###############################################################################

"""
PGF simulation

This file provides the main algorithm to compute a simulation of set of plans

It uses PGF_simulation_tools and PGF_simulation_conf, which provide necessary 
tools for the execution of the main algorithm:
- PGF_simulation_tools provides mathematic and kpi gain computation
- PGF_simulation_conf provides the means for validating and loading the
simulation inputs

For each KPI in a given plan we compute gain. We do it also for each group. The 
algorithm is as follows:
1. Determination of groups undergoing type 'a' policies.
2. Computation of definite KPI value and KPI gain for users undergoing type 'a'
policies
3. Determination of the amount of users not undergoing type 'a' policies
4. Computation of theoretical KPI value for users not undergoing type 'a'
policies
5. Determination of groups undergoing type 'b' policies
6. Determination of equivalent amount of users removed (lineal relation between
released bandwidth and theoretical removed users)
7. Computation of definite KPI value and KPI gain for users undergoing type 'b' 
policies (according to a lineal function): that excludes throttling policies set
to '3000' or traffic gating for the remaining services.
8. Computation of theoretical KPI value for users not undergoing type 'a' and
type 'b' policies
9. 

TO-DO LIST:
- Finish the header sections in each file:
  - PGF_simuation.py: ongoing
  - PGF_simulation_conf.py: done
  - PGF_simulation_tools.py: not started
- Clarify the use of 'per_one' and 'per_cent': done
- Introduce a different treatment of KPI computation depending on ratio: not started
- Document the algorithm in PGF_simulation.py: ongoing
- Document the functions in each file:
  - PGF_simulation_conf.py: done
  - PGF_simulation_tools.py: not started
- Update .docx document: ongoing
- Adapt to Django conventions: done
- Test with Django development server: ongoing
- Deploy Apache: not started
- Test with Apache: not started
- Create JS page able to plot a graph: ongoing
- Adapt output to JS page plotting grapths: ongoing
- Adapt input to REST: done
- include a logging feature: ongoing
- 

"""
import sys
import os.path

main_code_folder = "pgf_simulation_conf"
base_file_path = os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)), main_code_folder)
sys.path.insert(0, base_file_path)
from PGF_simulation_tools import normalized_kpi_gain, kpi_no_policies
from PGF_simulation_conf import control, get_preloaded_JSON, validate_config, validate_JSON_input, get_PGF_logger, store_JSON

import json
import uuid
from datetime import datetime
from time import gmtime, strftime
from copy import deepcopy
from multiprocessing import Pool

def main(request_payload=None, simulation_file_name=None, output=True) :
    # Definition of the level of logging. Output is True in regular regulation requests and False in recommendations
    if output == True :
        logger = get_PGF_logger()
    else :
        logger = get_PGF_logger('ERROR')
        
    # Multiprocessing
    pool = Pool()

    # First, we define the dictionary that will store the simulation results
    simulation_results = dict()                                                 # see simulation_output.json for further details
    simulation_results["id"] = str(uuid.uuid4())                                # a unique simulation identifier
    simulation_results["date"] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')   # simulation date
    simulation_results["results"] = list()                                      # per plan simulation results
 
    logger.info('[Simulation %s (%s)] starts' % (simulation_results["id"], simulation_results["date"]))
    
    # Variable for passing on results (consider using a class when refactoring; first item is a status, boolean, code; second one
    #is the actual result, if code is True)
    simulation_result = {"code": False, "payload": ''}

    # Configuration information validation and load
    try :
        conf_data = validate_config (logger)
        logger.info('.Configuration data loaded')
    except NameError as error:
        simulation_result["payload"] = ('Configuration validation file error: %s' %(error))
        logger.error('Configuration validation file error: %s' %(error))
        return simulation_result
    
    # Simulation request information validation and load
    if request_payload == None :
        # the simulator uses a preloaded JSON file (through get_example_JSON)
        try :
            JSON_input_data = get_preloaded_JSON (logger, simulation_file_name)
            logger.info('.Input data loaded (from preloaded file)')
        except NameError as error:
            simulation_result["payload"] = ('JSON file read operation error: %s' %(error))
            logger.error('JSON file read operation error: %s' %(error))
            return simulation_result
    else :
        # the simulator uses the input sent from the simulator application
        try :
            JSON_input_data = json.loads(request_payload)
            logger.info('.Input data loaded')
        except Exception as error:
            simulation_result["payload"] = ('JSON input not valid %s' %(error))
            logger.error('JSON input not valid %s' %(error))
            return simulation_result

    try :
        input_data = validate_JSON_input (JSON_input_data, conf_data, logger)
        logger.info('.Input data validated')
    except NameError as error:
        simulation_result["payload"] = ('JSON file validation error: %s' %(error))
        logger.error('JSON file validation error: %s' %(error))
        return simulation_result

    # request logging
    try :
        if output == True :
            pool.apply_async(func, args=(simulation_results["id"], JSON_input_data, 'input',))
            #store_JSON (simulation_results["id"], JSON_input_data, 'input')
            logger.info('.Simulation request stored in %s_input.log' % (simulation_results["id"]))
    except :
        logger.warning('Simulation request cannot be stored')
        pass
    
    # request_id extraction
    if 'source' in JSON_input_data and JSON_input_data["source"] == u'main' and 'request_id' in JSON_input_data :
        simulation_results["request_id"] = JSON_input_data["request_id"]
    
    # The following information is needed for starting a simulation:
    # - Subscriber group information (name, share) -> input_data["shares"]
    # - KPI to simulate -> input_data["services"]
    # - Policies to be applied to each subscriber groups (type, parameters, 
    # subscriber groups the policy applies to) -> input_data["plans"]
    # - Functions and parameters for computing KPI gain -> conf_data["general"]
    
    # Simulation of each plan (plan_name extracted from input_data["plans"])
    for plan_name in input_data["plans"] :
        # For each plan, we fill per_plan_result. At the end, we append it to 
        #simulation_results["results"]
        per_plan_result = dict()
        per_plan_result["plan"] = plan_name
        per_plan_result["groups"] = list() 
        per_plan_result["kpis"] = list()

        logger.info ('.Simulation of plan %s starts' %(plan_name))

        # In order to fill per_plan_result["groups"], we extract the 'actions' in each plan. 
        #An action defines which mitigation actions ('policy') undergoes each group ('group'). 
        #Actions can be applied according to different parameters ('parameters')
        for action in input_data["plans"][plan_name] :
            # Creation of temporary dictionaries to be stored later in per_plan_result["groups"] 
            #(it is a list of dictionaries). Each temporary dictionary stores results not 
            #tied to KPI gain computation, such as 'group', 'policy', 'share', 'susbscribers' 
            #and weights. Once computed, it is appended to per_plan_result["groups"]:
            # - intermediate_group_results: usual dictionary
            # - intermediate_wifigroup_results stores results for a fictitious group 
            #   (policy: 'no_wiki') for those users deemed to be switched to wifi but 
            #   without actual ANDSF support
            intermediate_group_results = dict()
            intermediate_wifigroup_results = dict()
    
            intermediate_group_results["group"] = deepcopy(action["group"])
            intermediate_group_results["policy"] = deepcopy(action["policy"])
            if ((action["policy"] == 'traffic_gating' or action["policy"] == 'bandwidth_throttling') and "parameters" in action) :
                intermediate_group_results["policy_parameters"] = deepcopy(action["parameters"])
            if action["policy"] == 'wifi' :
                intermediate_group_results["share"] = input_data["shares"][action["group"]] * float(conf_data["general"]["andsf_ratio"])/conf_data["general"]["ratio_factor"]
            else :
                intermediate_group_results["share"] = input_data["shares"][action["group"]]
            #### Comment: should the following statement follow the previous if?
            intermediate_group_results["subscribers"] = float(conf_data["general"]["max_users"]) * (intermediate_group_results["share"] / conf_data["general"]["ratio_factor"])
            intermediate_group_results["weights"] = conf_data["weights"][action["group"]]
            per_plan_result["groups"].append(intermediate_group_results)
    
            if action["policy"] == 'wifi' :
                intermediate_wifigroup_results["group"] = deepcopy(action["group"])
                intermediate_wifigroup_results["policy"] = 'no_wifi'
                intermediate_wifigroup_results["share"] = input_data["shares"][action["group"]] - intermediate_group_results["share"]
                intermediate_wifigroup_results["subscribers"] = float(conf_data["general"]["max_users"]) * (intermediate_wifigroup_results["share"] / conf_data["general"]["ratio_factor"])
                intermediate_wifigroup_results["weights"] = deepcopy(intermediate_group_results["weights"])
                #print extra_temp_results
                per_plan_result["groups"].append(intermediate_wifigroup_results)     #new
        
        # In order to fill per_plan_result["kpis"], we determine the services and KPIs that
        #must be simulated in each plan and simulate each of them
        kpis = [(service, kpi) for service in input_data["services"] for kpi in input_data["services"][service]]
        for service, kpi in kpis :
            logger.info('...Simulation of %s (%s) starts' % (kpi, service))
            # Creation of temporary dictionaries for the results associated to each KPI.
            #intermediate_kpi_results stores results tied to KPI gain computation. Once computed, 
            #it is appended to per_plan_result["kpis"]
            intermediate_kpi_results = dict()
            short_kpi_name = dict(control["kpi"]).keys()[dict(control["kpi"]).values().index(kpi)] # translation from long to short name
            intermediate_kpi_results["kpi_name"] = short_kpi_name
            intermediate_kpi_results["value_type"] = conf_data["general"]["global_kpi_ratio"]
            intermediate_kpi_results["kpi_groups"] = list()
            
            # Variables used to compute total KPI and weighted KPI
            total_kpi = 0.0
            total_weighted_kpi = 0.0
            
            if short_kpi_name in conf_data["supported_kpis"] :
                # ENHANCEMENT: verify what to do if this verification is negative
                subscribers_with_no_a = float(conf_data["general"]["max_users"])
                # 1. Determination of type 'a' policies (steps 1, 2 and 3)
                #---actions = [i for i in simulation_results[plan_name] if (i["policy"] == "wifi" or i["policy"] == "to3g")]
                actions = [i for i in per_plan_result["groups"] if (i["policy"] == "wifi" or i["policy"] == "to3g")]
                # 2. Computation of definite KPI value and KPI gain for users undergoing type 'a' policies
                for action in actions :
                    #print '+', result["policy"]
                    if action["policy"] == "wifi" : 
                        logger.info('.....Wifi policy found for group %s. Simulating' %(action["group"]))
                        new_kpi_value = float (conf_data["supported_kpis"][short_kpi_name]["wifi"])
                    elif action["policy"] == "to3g" : 
                        logger.info('.....Switch to 3G policy found for group %s. Simulating' %(action["group"]))
                        new_kpi_value = float (conf_data["supported_kpis"][short_kpi_name]["to3g"])
                    kpi_gain = normalized_kpi_gain (float (conf_data["supported_kpis"][short_kpi_name]["threshold"]), 
                                                    new_kpi_value, 
                                                    float (conf_data["supported_kpis"][short_kpi_name]["worst"]), 
                                                    float (conf_data["supported_kpis"][short_kpi_name]["best"]), 
                                                    float (conf_data["supported_kpis"][short_kpi_name]["threshold"]))
                    intermediate_kpi_results["kpi_groups"].append({"name":action["group"], "kpi_gain":kpi_gain, "value_type": conf_data["general"]["individual_kpi_ratio"]})
                    total_kpi = total_kpi + action["share"] * kpi_gain
                    total_weighted_kpi = total_weighted_kpi + (action["share"] * kpi_gain * int(action["weights"]) / max(int(i) for i in conf_data["weights"].values()))
                    
                    # 3. Determination of the amount of users not undergoing type 'a' policies
                    subscribers_with_no_a = subscribers_with_no_a - float(action["subscribers"])
    
                # 4. Computation of theoretical KPI value for users not undergoing type 'a' policies
                ##### COMMENT: If no type a users, a KPI value is received. Why? It's because the function does not exactly crosses the axis in the right point
                #intermediate_kpi_for_no_a = kpi_no_policies (conf_data["supported_kpis"][short_kpi_name], conf_data["general"]["kpi_gain_function"], conf_data["general"]["kpi_gain_parameter"], subscribers_with_no_a)
                intermediate_kpi_for_no_a = kpi_no_policies (conf_data, short_kpi_name, subscribers_with_no_a)
    
                # 5. Determination of groups undergoing type 'b' policies
                actions = [i for i in per_plan_result["groups"] if (i["policy"] == "bandwidth_throttling" or i["policy"] == "traffic_gating")]
                equivalent_removed_users = 0.0
                for action in actions :
                    #print "plan", plan, "policy", result["policy"], "group", result["group"]
                    if action["policy"] == "bandwidth_throttling" : 
                        logger.info('.....Bandwidth throttling policy found for group %s. Simulating' %(action["group"]))
                        action_definition = next((item for item in input_data["plans"][plan_name] if item["group"] == action["group"] and item["policy"] == action["policy"]), None)
                        bandwidth_limit = action_definition["parameters"]["limit"]
                        #bandwidth_limit = get_item("group", action["group"], "policy", action["policy"], 
                        #              input_data["plans"][plan_name])["parameters"]["limit"]
                        if bandwidth_limit == 3000 :
                            action["policy"] = "no_bandwidth_throttling" # in fact, this policy is considered a 'no policy'
                        elif bandwidth_limit == 1000 :
                            equivalent_removed_users = equivalent_removed_users + 2*action["subscribers"]/3     # as if 2/3 of users where removed
                            #print 'equivalent users', equivalent_removed_users
                            new_kpi_value = intermediate_kpi_for_no_a + (2*abs(float(conf_data["supported_kpis"][short_kpi_name]["best"])-intermediate_kpi_for_no_a)/3)         # new theoretical KPI value
                            #print 'new KPI value', new_kpi_value
                            kpi_gain = normalized_kpi_gain (float (conf_data["supported_kpis"][short_kpi_name]["threshold"]), 
                                                            new_kpi_value, 
                                                            float (conf_data["supported_kpis"][short_kpi_name]["worst"]), 
                                                            float (conf_data["supported_kpis"][short_kpi_name]["best"]), 
                                                            float (conf_data["supported_kpis"][short_kpi_name]["threshold"]))
                            intermediate_kpi_results["kpi_groups"].append({"name":action["group"], "kpi_gain":kpi_gain, "value_type": conf_data["general"]["individual_kpi_ratio"]})
                            total_kpi = total_kpi + action["share"] * kpi_gain
                            total_weighted_kpi = total_weighted_kpi + (action["share"] * kpi_gain * int(action["weights"]) / max(int(i) for i in conf_data["weights"].values()))
                            #print '       KPI value for bandwidth throttling policy for group '+result["group"]+' is: '+str(new_kpi_value)+' ('+str(result[short_kpi_name+"_gain"]*conf_general_dict ['ratio_factor'])+'%)'
                        elif bandwidth_limit == 64 :
                            equivalent_removed_users = equivalent_removed_users + action["subscribers"]         # as if all the users where removed
                            #print 'equivalent users', equivalent_removed_users
                            new_kpi_value = float (conf_data["supported_kpis"][short_kpi_name]["worst"])        # worst KPI value
                            #print 'new KPI value', new_kpi_value
                            kpi_gain = normalized_kpi_gain (float (conf_data["supported_kpis"][short_kpi_name]["threshold"]), 
                                                            new_kpi_value, 
                                                            float (conf_data["supported_kpis"][short_kpi_name]["worst"]), 
                                                            float (conf_data["supported_kpis"][short_kpi_name]["best"]), 
                                                            float (conf_data["supported_kpis"][short_kpi_name]["threshold"]))
                            intermediate_kpi_results["kpi_groups"].append({"name":action["group"], "kpi_gain":kpi_gain, "value_type": conf_data["general"]["individual_kpi_ratio"]})
                            total_kpi = total_kpi + action["share"] * kpi_gain
                            total_weighted_kpi = total_weighted_kpi + (action["share"] * kpi_gain * int(action["weights"]) / max(int(i) for i in conf_data["weights"].values()))
                            #print '       KPI value for bandwidth throttling policy for group '+result["group"]+' is: '+str(new_kpi_value)+' ('+str(result[short_kpi_name+"_gain"]*conf_general_dict ['ratio_factor'])+'%)'
                    elif action["policy"] == "traffic_gating" :
                        action_definition = next((item for item in input_data["plans"][plan_name] if item["group"] == action["group"] and item["policy"] == action["policy"]), None)
                        gated_services = action_definition["parameters"]["services"]
                        #gated_services = get_item("group", action["group"], "policy", action["policy"], 
                        #               input_data["plans"][plan_name])["parameters"]["services"]
                        logger.info('.....Traffic gating policy found for group %s (KPI simulation for service %s). Simulating' %(action["group"], service))
                        
                        for gated_service in gated_services :
                            if service == gated_service : equivalent_removed_users = equivalent_removed_users + (action["subscribers"]*float(conf_data["services"][service])/100)
                                
                        if service in gated_services :
                            logger.info('.....Traffic gating applies to services %s: match' %(', '.join(gated_services)))
                            new_kpi_value = float (conf_data["supported_kpis"][short_kpi_name]["worst"])        # worst KPI value
                            #print 'new KPI value', new_kpi_value
                            kpi_gain = normalized_kpi_gain (float (conf_data["supported_kpis"][short_kpi_name]["threshold"]), 
                                                            new_kpi_value, 
                                                            float (conf_data["supported_kpis"][short_kpi_name]["worst"]), 
                                                            float (conf_data["supported_kpis"][short_kpi_name]["best"]), 
                                                            float (conf_data["supported_kpis"][short_kpi_name]["threshold"]))
                            intermediate_kpi_results["kpi_groups"].append({"name":action["group"], "kpi_gain":kpi_gain, "value_type": conf_data["general"]["individual_kpi_ratio"]})
                            total_kpi = total_kpi + action["share"] * kpi_gain
                            total_weighted_kpi = total_weighted_kpi + (action["share"] * kpi_gain * int(action["weights"]) / max(int(i) for i in conf_data["weights"].values()))
                            #print '       KPI value for traffic gating policy ('+service+') for group '+result["group"]+' is: '+str(new_kpi_value)+' ('+str(result[short_kpi_name+"_gain"]*conf_general_dict ['ratio_factor'])+'%)'
                        else :
                            logger.info('.....Traffic gating applies to services %s: no match' %(', '.join(gated_services)))
                            action["policy"] = "no_traffic_gating"
                            #print '       KPI value for traffic gating policy for group '+result["group"]+' cannot be computed yet'
                # Intermediate KPI value for uses that do not undergo type 'a' or 'b' policies
                subscribers_with_no_b = subscribers_with_no_a - equivalent_removed_users
                ##### COMMENT: shouldn't this function consider the starting point of the KPI enhancement?
                #intermediate_kpi_for_no_b = kpi_no_policies (conf_data["supported_kpis"][short_kpi_name], conf_data["general"]["kpi_gain_function"], conf_data["general"]["kpi_gain_parameter"], subscribers_with_no_b)
                intermediate_kpi_for_no_b = kpi_no_policies (conf_data, short_kpi_name, subscribers_with_no_b)
                #print 'intermediate kpi', intermediate_kpi_for_no_b
                
                actions = [i for i in per_plan_result["groups"] if (i["policy"] == "video_acceleration")]
                for action in actions :
                    logger.info('.....Video acceleration gating policy found for group %s. Simulating' %(action["group"]))
                    ##### COMMENT: shouldn't have equivalent_removed_users been resetted?
                    ##### COMMENT: verify whether the multiplication below is right!!!
                    equivalent_removed_users = equivalent_removed_users + (action["subscribers"]*float(conf_data["general"]['video_acc_saving_ratio'])/100)
                    kpi_gain = normalized_kpi_gain (float (conf_data["supported_kpis"][short_kpi_name]["threshold"]), 
                                                    intermediate_kpi_for_no_b, 
                                                    float (conf_data["supported_kpis"][short_kpi_name]["worst"]), 
                                                    float (conf_data["supported_kpis"][short_kpi_name]["best"]), 
                                                    float (conf_data["supported_kpis"][short_kpi_name]["threshold"]))
                    intermediate_kpi_results["kpi_groups"].append({"name":action["group"], "kpi_gain":kpi_gain, "value_type": conf_data["general"]["individual_kpi_ratio"]})
                    total_kpi = total_kpi + action["share"] * kpi_gain
                    total_weighted_kpi = total_weighted_kpi + (action["share"] * kpi_gain * int(action["weights"]) / max(int(i) for i in conf_data["weights"].values()))
                    #print '       KPI value for policy '+result["policy"]+' for group '+result["group"]+' is '+str(intermediate_kpi_for_no_b)+' ('+str(result[short_kpi_name+"_gain"]*conf_general_dict ['ratio_factor'])+'%)'
    
                #intermediate_kpi_for_no_c = kpi_no_policies (conf_data["supported_kpis"][short_kpi_name], conf_data["general"]["kpi_gain_function"], conf_data["general"]["kpi_gain_parameter"], subscribers_with_no_a - equivalent_removed_users)
                intermediate_kpi_for_no_c = kpi_no_policies (conf_data, short_kpi_name, subscribers_with_no_a - equivalent_removed_users)
                for action in per_plan_result["groups"] :
                    if action["policy"] == 'no_wifi' or action["policy"] == 'no_traffic_gating' or action["policy"] == 'no_bandwidth_throttling' or action["policy"] =="no_policy":
                        kpi_gain = normalized_kpi_gain (float (conf_data["supported_kpis"][short_kpi_name]["threshold"]), 
                                                        intermediate_kpi_for_no_c, 
                                                        float (conf_data["supported_kpis"][short_kpi_name]["worst"]), 
                                                        float (conf_data["supported_kpis"][short_kpi_name]["best"]), 
                                                        float (conf_data["supported_kpis"][short_kpi_name]["threshold"]))
                        intermediate_kpi_results["kpi_groups"].append({"name":action["group"], "policy":'No', "kpi_gain":kpi_gain, "value_type": conf_data["general"]["individual_kpi_ratio"]})
                        total_kpi = total_kpi + action["share"] * kpi_gain
                        total_weighted_kpi = total_weighted_kpi + (action["share"] * kpi_gain * int(action["weights"]) / max(int(i) for i in conf_data["weights"].values()))
    
                #intermediate_kpi_results["delta_value"] = '%s' % (total_kpi)
                #intermediate_kpi_results["weighted_delta_value"] = '%s' % (total_weighted_kpi)
                intermediate_kpi_results["delta_value"] = total_kpi
                intermediate_kpi_results["weighted_delta_value"] = total_weighted_kpi
                per_plan_result["kpis"].append(intermediate_kpi_results)
            else  :
                logger.warning('KPI % not supported. Not computed' % (kpi))
                #logging.warning('KPI % not supported. Not computed' % (kpi))

            logger.info('...Simulation of %s (%s) ends' % (kpi, service))
            
        # the processing for this plan is over. Let's append the per plan results to simulation_results["results"]
        simulation_results["results"].append(per_plan_result)
        #print '       KPI value for policy '+result["policy"]+' for group '+result["group"]+' is '+str(intermediate_kpi_for_no_c)+' ('+str(result[short_kpi_name+"_gain"]*conf_general_dict ['ratio_factor'])+'%)'
        logger.info ('.Simulation of plan %s ends' %(plan_name))
    
    # simulation results are sent back
    if request_payload == None :
        # if simulation is carried out on a preloaded file, say it!!
        simulation_results["remarks"] = u'The results passed on by this JSON file have been generated by a preloaded plan description and do not come from any mitigation plan simulation request'

    logger.info('[Simulation %s (%s)] ends\n\n' % (simulation_results["id"], simulation_results["date"]))
    if output == True :
        print json.dumps(simulation_results,indent=4)
    
    try :
        if output == True :
            store_JSON (simulation_results["id"], simulation_results, 'output')
            logger.info('.Simulation results stored in %s_output.log' % (simulation_results["id"]))
    except :
        logger.warning('Simulation results cannot be saved')
        pass

    simulation_result["code"] = True
    simulation_result["payload"] = json.dumps(simulation_results,indent=4)
    return simulation_result

if __name__ == "__main__":
    main()