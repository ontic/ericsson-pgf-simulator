////////////////////////////////////////////////////////////////////////////////
////                                                                        //// 
//// COPYRIGHT (c) Ericsson AB 2015                                         //// 
////                                                                        //// 
//// The copyright to the computer Program(s) herein is the                 //// 
//// property of Ericsson AB, Sweden. The program(s) may be                 //// 
//// used and or copied only with the written permission of                 //// 
//// Ericsson AB, or in accordance with the terms and conditions            //// 
//// stipulated in the agreement contract under which the                   //// 
//// program(s) have been supplied.                                         //// 
////                                                                        //// 
////////////////////////////////////////////////////////////////////////////////

//// Variables
var config_options = {};            // Variable storing the configuration info

//// Validation and submission functions

// validation + value filling function
function validate_admin_form () {
    // validate that a KPI gain function is selected
    var has_function = false;
    $('.btn-group[id="function_type"] button').each( function () {
        if ($(this).hasClass('active')) {
            has_function = true;
            config_options["kpi_gain_function"] = $(this).text()
            return false;
        }
    });
    if (has_function === false) {
        alert ('Please, choose a gain function');
        return false;
    }
    
    // validate that every ratio has a chosen button
    var has_ratio = false;
    $('.btn-group[id="user_share_ratio"] button').each( function () {
        if ($(this).hasClass('active')) {
            has_ratio = true;
            config_options["user_share_ratio"] = $(this).text().charAt(0).toUpperCase() + $(this).text().substring(1);
            return false;
        }
    });
    if (has_ratio === false) {
        alert ('Please, choose a user share ratio');
        return false;
    } else {
        has_ratio = false;
    }
    
    $('.btn-group[id="kpi_gain_ratio"] button').each( function () {
        if ($(this).hasClass('active')) {
            has_ratio = true;
            config_options["individual_kpi_ratio"] = $(this).text().charAt(0).toUpperCase() + $(this).text().substring(1);
            return false;
        }
    });
    if (has_ratio === false) {
        alert ('Please, choose a KPI gain ratio');
        return false;
    } else {
        has_ratio = false;
    }

    $('.btn-group[id="global_gain_ratio"] button').each( function () {
        if ($(this).hasClass('active')) {
            has_ratio = true;
            config_options["global_kpi_ratio"] = $(this).text().charAt(0).toUpperCase() + $(this).text().substring(1);
            return false;
        }
    });
    if (has_ratio === false) {
        alert ('Please, choose a global KPI gain ratio');
        return false;
    }        

    // validate that the ANDSF ratio is set
    if (check_ranges ($('#andsf_ratio'), 'ANDSF ratio', config_options['user_share_ratio']) ) {
        config_options["andsf_ratio"] = parseFloat($('#andsf_ratio').val());
    } else {
        $('#andsf_ratio').focus();
        return false;
    }

    // validate that video acceleration enhancement ratio is set
    if (check_ranges ($('#video_acceleration_ratio'), 'Video Acceleration enhancement ratio', config_options['user_share_ratio']) ) {
        config_options["video_acceleration_ratio"] = parseFloat($('#video_acceleration_ratio').val());
    } else {
        $('video_acceleration_ratio').focus();
        return false;
    }

    // validate that traffic mix is set
    var total_traffic = 0.0;
    var has_traffic_mix = true;
    config_options["services"] = {};
    var i = 0;
    $('#traffic_mix input').each( function () {
        //console.log ($(this).parent().find(">:first-child").text() + ' traffic ratio');
        if ( check_ranges ($(this), $(this).parent().find(">:first-child").text() + ' traffic ratio', config_options['user_share_ratio']) ) {
            total_traffic = total_traffic + parseFloat($(this).val());
            config_options["services"][$(this).attr('id').replace('_share', '')] = parseFloat($(this).val());
        } else {
            $(this).focus();
            has_traffic_mix = false;
            return false;
        }
    });
    if (has_traffic_mix === true) {
        if ( (config_options["user_share_ratio"] === 'Per one') && (total_traffic != 1.0) ) {
            alert ('Traffic mix total must be 1.0');
             $('#traffic_mix input').first().focus();
            return false;
        } else if ( (config_options["user_share_ratio"] === 'Per cent') && (total_traffic != 100.0) ) {
            alert ('Traffic mix total must be 100.0');
             $('#traffic_mix input').first().focus();
            return false;
        }
    }

    // validate that KPI limits are set
    config_options["kpis"] = {};
    $('#kpi_table tr[id]').each( function () {
        config_options["kpis"][$(this).attr('id').replace('_form', '')] = {};
        //alert ($(this).attr('id'));
        var i = 0;
        var kpi_id = $(this).attr('id').replace('_form', '');
        $(this).find('input:not(:disabled)').each (function () {
            if (i === 0) {
                config_options["kpis"][kpi_id]["wifi"] = parseFloat($(this).val());
            } else {
                config_options["kpis"][kpi_id]["to3g"] = parseFloat($(this).val());
            }
            i++;
            //console.log($(this).val());
        });
    });
    
    // validate that ser group weights are set
    var has_weights = true;
    config_options["groups"] = {};
    $('.btn-group[id$="_weight"]').each( function () {
        var group_name = $(this).attr('id').replace('_weight', '');
        $(this).children("button").each (function () {
            this_group_has_weights = false;
            if ($(this).hasClass('active')) {
                config_options["groups"][group_name] = $(this).text();
                this_group_has_weights = true;
                return false;
            }
        });
        if (this_group_has_weights === false) {
            has_weights = false;
            return false;
        }
    });
    if (has_weights === false) {
        alert ('Please, assign a weight to all the customer groups');
        return false;
    }

    // record the skin selection
    config_options["skin"] = $('input[name=skin]:checked').val();

    // everything was OK
    return true;
}

//// Event handling

// function that shows the initial form values from stored_configuration_JSON. It handles the event ready associated
//to the document
var show_initial_values = function (stored_configuration) {

    // make the KPI gain function active
    var kpi_gain_function = stored_configuration["general"]["kpi_gain_function"].charAt(0).toUpperCase() + stored_configuration["general"]["kpi_gain_function"].substring(1);
    var function_button_id = '#function_type';
    $(function_button_id + ' button:contains("' + kpi_gain_function +'")').addClass('active');

    // set KPI gain function parameters
    switch (stored_configuration["general"]["kpi_gain_function"]) {
        case 'linear':
        case 'logistic':
        case 'exponentiation':
            $('#input_log_parameter').text(stored_configuration["general"]["kpi_gain_parameter"]);
            $('#input_exp_parameter').text(stored_configuration["general"]["kpi_gain_parameter_2"]);
            break;
    }
    // set available policies
    $.each (stored_configuration["policies"], function (index, value) {
        var div_html_lead = '<div class="checkbox disabled">\n';
        var div_html_end = '</label>\n \
                            </div>';
        var div_html_body = '';
        var div_html = '';
        
        if (value === 'wifi') {
            div_html_body = '<label>\n \
                                <input type="checkbox" value="" checked disabled>\n \
                                Wifi Off-loading\n';         
        } else if (value === 'to3g') {
            div_html_body = '<label>\n \
                                <input type="checkbox" value="" checked disabled>\n \
                                Switch to 3G\n';         
        } else if (value === 'traffic_gating') {
            div_html_body = '<label data-toggle="tooltip" data-placement="left" title="There are three different types of traffic: Web Browsing, File Transfer and Video. Each of them can be banned/allowed.">\n \
                                <input type="checkbox" value="" checked disabled>\n \
                                Traffic Gating\n';         
        } else if (value === 'bandwidth_throttling') {
            div_html_body = '<label data-toggle="tooltip" data-placement="top" title="Three throttling thresholds are supported: no throttling (3 Mbps), total traffic banning (64 kbps), and partial throttling (1 Mbps)">\n \
                                <input type="checkbox" value="" checked disabled>\n \
                                Bandwidth Throttling\n';         
        } else if (value === 'video_acceleration') {
            div_html_body = '<label>\n \
                                <input type="checkbox" value="" checked disabled>\n \
                                Video Accelerattion\n';         
        }
        div_html = div_html_lead + div_html_body + div_html_end;
        $('#policy_list').append(div_html)
    });
    
    // set user shares
    if (parseFloat(stored_configuration["general"]["ratio_factor"]) === 100.0) {
        $('#user_share_ratio button:contains("Percentage")').addClass('active');
    } else {
        $('#user_share_ratio button:contains("Unit")').addClass('active');
    }
    if (stored_configuration["general"]["individual_kpi_ratio"] === "per_cent") {
        $('#kpi_gain_ratio button:contains("Percentage")').addClass('active');
    } else {
        $('#kpi_gain_ratio button:contains("Unit")').addClass('active');
    }
    if (stored_configuration["general"]["global_kpi_ratio"] === "per_cent") {
        $('#global_gain_ratio button:contains("Percentage")').addClass('active');
    } else {
        $('#global_gain_ratio button:contains("Unit")').addClass('active');
    }
    
    // set ANDSF share
    $('#andsf_ratio').val(parseFloat(stored_configuration["general"]["andsf_ratio"]).toFixed(2));

    // set the service mix shares
    $('#video_share').val(parseFloat(stored_configuration["services"]["video"]).toFixed(2));
    $('#web_browsing_share').val(parseFloat(stored_configuration["services"]["web_browsing"]).toFixed(2));
    $('#file_transfer_share').val(parseFloat(stored_configuration["services"]["file_transfer"]).toFixed(2));
    
    // set video acceleration rate
    $('#video_acceleration_ratio').val(parseFloat(stored_configuration["general"]["video_acc_saving_ratio"]).toFixed(2));
    
    // set the KPI thresholds
    $('#vfr_form input:eq(0)').val(parseFloat(stored_configuration["supported_kpis"]["vfr"]["best"]).toFixed(2));
    $('#vfr_form input:eq(1)').val(parseFloat(stored_configuration["supported_kpis"]["vfr"]["threshold"]).toFixed(2));
    $('#vfr_form input:eq(2)').val(parseFloat(stored_configuration["supported_kpis"]["vfr"]["worst"]).toFixed(2));
    $('#vfr_form input:eq(3)').val(parseFloat(stored_configuration["supported_kpis"]["vfr"]["wifi"]).toFixed(2));
    $('#vfr_form input:eq(4)').val(parseFloat(stored_configuration["supported_kpis"]["vfr"]["to3g"]).toFixed(2));

    $('#va_form input:eq(0)').val(parseFloat(stored_configuration["supported_kpis"]["va"]["best"]).toFixed(2));
    $('#va_form input:eq(1)').val(parseFloat(stored_configuration["supported_kpis"]["va"]["threshold"]).toFixed(2));
    $('#va_form input:eq(2)').val(parseFloat(stored_configuration["supported_kpis"]["va"]["worst"]).toFixed(2));
    $('#va_form input:eq(3)').val(parseFloat(stored_configuration["supported_kpis"]["va"]["wifi"]).toFixed(2));
    $('#va_form input:eq(4)').val(parseFloat(stored_configuration["supported_kpis"]["va"]["to3g"]).toFixed(2));
    
    // set the customer group weights
    $.each( stored_configuration["weights"], function( group, group_weight ) {
        button_id = '#' + group + '_weight';
        $(button_id + ' button:contains("' + group_weight +'")').addClass('active');
    });
    
    // set the skin
    $(":radio[value="+stored_configuration["general"]["skin"]+"]").prop("checked", true);
    
    // set the policy limits
    $('#wifi_limit').val(parseFloat(stored_configuration["policy_limits"]["wifi"]).toFixed(0));
    $('#to3g_limit').val(parseFloat(stored_configuration["policy_limits"]["to3g"]).toFixed(0));
    $('#video_acceleration_limit').val(parseFloat(stored_configuration["policy_limits"]["video_acceleration"]).toFixed(0));
}

// function to handle JSON submission. It handles the click event associated to '#submit_button'
var submit_conf_info = function () {
    if (validate_admin_form()) {
        JSON_string = JSON.stringify(config_options);
        //$('#json').text(JSON.stringify(config_options));
        var output = $.ajax ({
                type: 'POST',
                url: MANAGE_CONFIG_URL,
                data: JSON.stringify(config_options), 
                contentType: 'application/json',
        });
        output.done (function (data) {
            $('#control_container').css ('display', 'none');
            $('#tab_container').css ('display', 'none');
            $('#tabs_container').css ('display', 'none');
            $('#json').css ('display', 'none');
            $('#success_container').css ('display', 'block');
        });
        output.fail (function (xhr, ajaxOptions, thrownError) {
            alert("Error: " + xhr.status + "\n" +
                "Message: " + xhr.statusText + "\n" +
                "Response: " + xhr.responseText + "\n" + thrownError);
        });
    }
}

//// Auxiliary functions

// function to validate the value is in a given numeric range
function check_ranges (object_reference, share_name, share_type) {
    var share = object_reference.val();
    if (parseFloat(share) === 0.0 || share === undefined) {
        alert ('Please, insert a non zero value for the ' + share_name);
        return false;
    } else if (parseFloat(share) < 0.0) {
        alert ('Please, insert a non negative value for the ' + share_name);
        return false;
    } else if ( (share_type === 'Per one') && (parseFloat(share) > 1.0) ) {
        alert ('Please, ' + share_name + ' values must be between 0.0 and 1.0');
        return false;
    } else if ( (share_type === 'Per cent') && (parseFloat(share) > 100.0) ) {
        alert ('Please, ' + share_name + ' values must be between 0.0 and 100.0');
        return false;
    } else {
        return true;
    }
}
